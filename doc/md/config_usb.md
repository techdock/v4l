### Introduction

This document does explane how to config the device with an USB-storage.

For more details check https://techdock.ch/de/downloads/documents

[TOC]

#### USB-storage

- the USB-storage need to be formated with FAT32 file system
- the USB-storage need to have a folder **vpn4log*** according parameter PA51

#### Parameter

Config device with USB-storage is depending of the follow parameters.

- min: smallest possible string
- std: value when delivered or after use function code FC32 on device
- max: biggest possible string

| PA| Function | min | std | max |
|--------|--------|--------|--------|--------|
| 51 | Folder names for USB | NULL | control4log | String[256] |
| 52 | Password for USB parameter upload | NULL | 456852 | String[256] |

#### Usage

As soon the USB-storage isplug into one of the USB-interface, the software is looking in folder **vpn4log*** for text files named as follow:

| File name | Function describtion |
|--------|--------|
| paradown | File is trigger the download of **confdown.csv from** device. The file contains all parameters of the device. |
| confdown.csv | Result file when download configuration with file **paradown**.|
| paraup | File is trigger the upload of **confup.csv to** device. The device is rebooting three time by self according the function codes FC33, FC34 and FC99. |
| textdown | File is trigger the download of **textdown.csv from** device. The file contains all test parameters of the device. |
 textdown.csv | Result file when download configuration with file **paradown**.|
| textup | File is trigger the upload of **textup.csv to** device. The device is rebooting three time by self according the function codes FC33, FC34 and FC99. |
| confup.csv | Parameters of the device will change according the file **confup.csv** if file **passwd.csv** is exisitng. Best is to copy downloaded file **confdown.csv**, adapte parameters and rename it to **confup.csv** |
| textup.csv | Text parameters of the device will change according the file **testup.csv** if file **passwd.csv** is exisitng. Best is to copy downloaded file **textdown.csv**, adapte parameters and rename it to **textup.csv** |
| passwd.csv | This file need to contain the password for upload according parameter PA52 |

For get all files, download [control4log Files for USB interfaces](https://www.techdock.ch/en/downloads/documents/control4log/id:3691228c7f94f970818a51f1f33e076c) from [Homepage](https://techdock.ch).

#### Download configuration from device

For download the configuration do the following:

1. Check if USB-stick is formated in FAT32 file system
2. Make sure folder **vpn4log** is empty
3. Copy the file **paradown** into the folder **control4log**
4. Input USB-stick into a free USB-interface
5. Wait approx. 2 min. and then pull the USB-stick
6. Now you will find the file **confdown.csv** in folder **vpn4log**

#### Upload configuration to device

For upload a new configuration to device do the following:

1. Make sure folder **control4log** is empty
2. Copy file **confdown.csv** and rename it to **confup.csv**
3. Adapte parameters as need
4. Copy file **confup.csv** and **passwd.csv** into folder **vpn4log**
5. Input USB-stick into a free USB-interface
6. Wait approx. 5 min. and then pull the USB-stick, device is rebooting three time by self according the function codes FC33, FC34 and FC99.

or download the text-parameter do the following:

1. Check if USB-stick is formated in FAT32 file system
2. Make sure folder **vpn4log** is empty
3. Copy the file **textdown** into the folder **control4log**
4. Input USB-stick into a free USB-interface
5. Wait approx. 2 min. and then pull the USB-stick
6. Now you will find the file **textdown.csv** in folder **vpn4log**

#### Upload configuration to device

For upload a new text-parameter to device do the following:

1. Make sure folder **vpn4log** is empty
2. Copy file **textdown.csv** and rename it to **textup.csv**
3. Adapte parameters as need
4. Copy file **textup.csv** and **passwd.csv** into folder **vpn4log**
5. Input USB-stick into a free USB-interface
6. Wait approx. 5 min. and then pull the USB-stick, device is rebooting three time by self according the function codes FC33, FC34 and FC99.

For check if text-parameters where writen to the device, do again download the configuration from device.

#### Example

Before download:

![usb_before.png](./usb_before.png)


After download:

![usb_after.png](./usb_after.png)

#### Info

- Download configuration and text-parameter **from** device can be done at the same time
- Upload configuration and text-parameter **to** device can be done at the same time
- Download and upload can **not** be done at the same time
- For more options check documentaation from device at [Homepage](https://techdock.ch/de/downloads/documents)