#### Introduction

This document does explane how to config the device with software through the ETH interfaces.

For more details check https://techdock.ch/de/downloads/documents

#### Links

How to: https://www.howtoraspberry.com/2021/06/how-to-send-mail-from-a-raspberry-pi/

OpenPGP Doku: https://www.gnupg.org/documentation/manuals/gnupg/OpenPGP-Key-Management.html

[TOC]

#### Passwort Email verschlüsseln

Create backup of file "msmtprc"

```plaintext
sudo cp /etc/msmtprc /etc/msmtprc.backup
```

Generate key ...

```plaintext
gpg --batch --passphrase '' --quick-gen-key my@email.ch default default never
```

Info: The keybox can be found in follow directory:

> ```plaintext
> /home/c4l/.gnupg/
> ```

Look at key

```plaintext
gpg --list-secret-keys --keyid-format LONG
```

Delete key:

```plaintext
gpg --delete-secret-key my@email.ch
```

Encrypt email-account password with generated key:

```plaintext
gpg --encrypt --output=/home/c4l/msmtp-password.gpg --recipient=my@email.ch
```

A empty line is shown where you need to enter the password and accept it by push buttton Enter ...

```plaintext
<Password> & Enter
```

... and then finish encryption process with the follow button combination ...

```plaintext
Ctrl & d
```

Check ig file msmtp-password.gpg in directory /home/c4l/ existing:

```plaintext
ls -la
```

Check password in file ...

```plaintext
gpg --decrypt /home/c4l/msmtp-password.gpg
```

... or write it in text file with:

```plaintext
gpg --decrypt /home/c4l/msmtp-password.gpg > /home/c4l/msmtp-password.txt
```

Adapte file /etc/msmtprc through PA's from device

```plaintext
FC01
PA138.gpg --decrypt /home/c4l/msmtp-password.gpg
FC99
```

... and check if in file the key "password" is replaced by the string from PA138 ...

```plaintext
sudo nano /etc/msmtprc
```

In the file /etc/msmtprc the keyword "password" needs to be replaced by "passwordeval":

old:

```plaintext
<password in klartext>
```

new:

```plaintext
passwordeval gpg --decrypt /home/c4l/msmtp-password.gpg
```

Info: If behind tag ...

```plaintext
password
```

... nothing is writen, automatically the tyg ...

```plaintext
passwordeval
```

... is used!

Sending email can be tested by the follow command :

```plaintext
echo "BEEP BEEP" | mailx -s "Subject: This is a test!" my@email.ch
```

#### Info

- For more options check documentaation from device at [Homepage](https://techdock.ch/de/downloads/documents)