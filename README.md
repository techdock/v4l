##### Index

[TOC]

<!---new page--->
<div style="page-break-before:always"></div>

##### Documentation

The follow additional software are neccessary to open and edit documentation files:

|Software|Path file(-s)|
|--------|--------|
|LibreOffice |./projects/v4l/doc/software |
|FreeCAD |./projects/v4l/doc/mechanics |
|Geany |./projects/v4l/src |

### Install Raspberry Pi OS

Install the `Raspberry Pi OS Full (32-bit), version 11 a port of Debain Bullseye` or `Raspberry Pi OS (Legacy), version 10 a port of Debain Buster` on a MicroSD accorrding https://www.raspberrypi.com/software/. Preferable the MicroSD Card is designed for extended writing cycles, 8GB will be enough without desktop, 32GB are recommended when install with desktop.

To be sure the project v4l is running after first installation the IPv4 address from eth0 need to be changed to static IP 192.168.19.77 according section **Set IP**.

### Set IP

> Open dhcpcd.conf
> ```
> sudo nano /etc/dhcpcd.conf
> ```

> Change below the index 'interface eth0' the settings as follow:
> ```
> static ip_address=192.168.19.77/24
> static routers=192.168.19.1
> static domain_name_servers=192.168.19.1
> ```
> ... or use Desktop-GUI if Raspberry Pi OS **Desktop** is installed.


### Set root password

> ```
> sudo -i
> passwd
> exit
> ```


### Setup Raspberry Pi OS


##### Prepare the OS

> ```
> sudo apt-get update
> ```


##### Install neccessary packet

> The follow additional packet are neccessary to operate this project:

> MariaDB
> ```
> sudo apt-get install default-mysql-server
> ```

> Metapaket MySQL
> ```
> sudo apt-get install default-libmysqlclient-dev
> ```

> Create and manage bridge devices
> ```
> sudo apt-get install bridge-utils
> ```

> SMTP client
> ```
> sudo apt-get install bsd-mailx msmtp msmtp-mta
> ```

> GIT
> ```
> sudo apt install git
> ```


### Install repository v4l

> Info: do not use `sudo` for command **mkdir** and **git** because then it belongs to root!

> ```
> mkdir ./projects/
> cd ./projects/
> git clone https://gitlab.com/techdock-gmbh/v4l.git
> ```


### Prepare database

> Info: user and password are hard coded in the follow file and can be customized if wished

> > db.h row 93
> > ```
> > sudo nano ./projects/v4l/src/db/db.h
> > ```


> Install security update for database

> > ```
> > sudo mysql_secure_installation
> > ```

> Answer questions as follow:
> > Enter current password for root: **input root passsword from section **Set root password**
> > 
> > Switch to unix_socket authentication: **n**
> > 
> > Change the root password? **y**
> > 
> > Remove anonymous users? **y**
> > 
> > Disallow root login remotely? **y**
> > 
> > Remove 'test' database? **y**
> > 
> > Reload privilege tables now? **y**


> Create user `trimada` in database

> > Log in database
> > ```
> > sudo mysql -u root
> > ```

> > Create user `trimada` and set password
> > ```
> > create user trimada@localhost;
> > grant all privileges on *.* to 'trimada'@'localhost' identified by 'welcome';
> > SET PASSWORD FOR 'trimada'@'localhost' = PASSWORD('Master-5610');
> > exit
> > ```


### Enter projects source directory

> Info: the `./` represents the relative path `/home/<user>/`

> > ```
> > cd ./projects/v4l/src
> > ```


### Stop services and build binaries 

> Info 1: delete and build binaries new is just neccessary if user and password of database was customized

> Info 2: service usbevent and tcpss need to be owned by current sudoer user and not root, check out variable `user` in script

> > Build binaries
> >
> > ```
> > sudo bash project-build.sh
> > ```


### Install project

> Info: copies all needed projects file to the neccesary destinations

> >
> > Install project 
> > ```
> > sudo bash project-install.sh
> > ```


### Clean project

> Info: clean project is just a nice to have. The script will remove all build files and not mandatory for running the project.

> > Clean project
> >
> > ```
> > sudo bash project-clean.sh
> > ```


### Start project

> Info: start project will add needed v4l service to systemctl list and start all of them

> > Clean project
> >
> > ```
> > sudo bash project-start.sh
> > ```


### Restart and write eth standard settings

Info: standard IPv4 settings are comming from `/home/v4l/projects/v4l/src/db/11007-PA-default.csv` in column `value`. Based at `11007-PA-default.csv` the table `PA` in database is build and can be access by query `SELECT * FROM PA;`

|PA|standard value|meaning|
|--------|--------|--------|
|101|192.168.19.77|IPv4 for eth0|

_More information about parameters at https://techdock.ch/de/downloads/control4log_


> Prepare database for restart
> ```
> mysql -u trimada -p
> ```
> Passwort: `Master-5610`

> Choose database
> ```
> use testBase
> ```

> Set flag for `FC33`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 3;
> ```

> Set flag for `FC34`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 4;
> ```

> Restart with `FC99`
> ```
> UPDATE FLAG SET value = 1 WHERE id = 5;
> ```

> The Raspiberry Pi will start several times to write all data 


### Check status of services after rebooting

> **tcpss**
> ```
> sudo systemctl status tcpss.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/v4l/projects/v4l/src/tcpss/tcpss.service; enabled; vendor preset: enabled)_
> 
> > _Active: inactive (dead) ..._


> **usbevent**
> ```
>  sudo systemctl status usbevent.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/v4l/projects/v4l/src/tools/usbevent/usbevent.service; enabled; vendor preset: enabled)_
> 
> > _Active: active (running) ..._


> **shutdown**
> ```
>  sudo systemctl status ioLog.service
> ```
> Result:
> 
> > _Loaded: loaded (/home/v4l/projects/v4l/src/tools/shutdown.service; enabled; vendor preset: enabled)_
> 
> > _Active: active (running) ..._


> **Alternative** 
> 
> all running services by root can be displayed with ...
> 
> ```
> ps -U root -u root -N
> ```
>
> all services as tree can be displayed with ...
>
> ```
> ps axjf
> ```
>
> all running services by user can be displayed with ...
>
> ```
> ps -eo euser,ruser,suser,fuser,f,comm,label
> ```
> 
> The list should show:
> 
> _tcpss_
> 
> _ioLog_
> 
> _usbevent_


### v4l-Wikis

[Config device with USB flash drive](https://gitlab.com/techdock/v4l/-/wikis/Config-device-with-USB-storage)

[Config device device with ethernet](https://gitlab.com/techdock/v4l/-/wikis/Config-device-with-ETH-interface)

[Encrypt SMTP password](https://gitlab.com/techdock/v4l/-/wikis/Encrypt-SMTP-password)


### Uninstall project

> > **Enter projects source directory**
> > ```
> > cd ./projects/v4l/src
> > ```

> > **Stop services**
> >
> > ```
> > sudo bash project-stop.sh
> > ```

> > **Delete all build files in project**
> >
> > ```
> > sudo bash project-clean.sh
> > ```

> > **Delete all build binaries**
> >
> > ```
> > sudo bash project-uninstall.sh
> > ```