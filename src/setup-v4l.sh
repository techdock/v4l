#! /bin/bash

# file          setup-v4l.sh
# brief         script for project vpn4log
# details       starting all running services from project
# date          14.05.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       14.05.2023      mimi    - creating
#

# declare script variables and dierctories
project="vpn4log"
name="v4l"
filename=$(basename $0)
user=$(whoami)
todfile=$(date +'%d.%m.%Y_%H:%M:%S')
tod=$(date)
ver=$(mysql --database=testBase --user=trimada --password=Master-5610 -s -N --execute="SELECT value FROM PA WHERE id = 0;")
pw1=""

printf "\n### Running ${filename} to start project ${project}, v${ver} services as user ${user} at ${tod}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:\t\t${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Source directory is:\t\t${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Project directory is:\t\t${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}" # will get back current directory where this skript is running
printf "Binary directory is:\t\t${bindir}\n"

# navigate to homedir /home/<user>/
cd prodir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../../ > /dev/null 2>&1 # redirect output for make cmd silent

# create homedir = /home/<user>/
homedir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Home directory is:\t\t${homedir}\n"

# create docdir = /home/<user>/Documents/
docdir="${homedir}/Documents"
printf "Documents directory is:\t\t${docdir}\n"

# create backdir = /home/<user>/Documents/<ver>-backup/
backdir="${docdir}/${ver}-backup"
printf "Backup directory is:    ${backdir}\n"

# starting process
printf "\n### starting setup for ${project}\n\n"


## backup 

### project
sudo cp -r $prodir $backdir/$name.$todfile

### MariaDB
mysqldump --user=trimada --password=Master-5610 testBase > $backdir/testBase.sql.$todfile


## set new password

### enter new password at CLI
pwckeck="false"
while [ "$pwcheck" != "true" ]; do
 pw2=""

 printf "Input password for device:"
 stty -echo # deactivate echo, password is not shown
 read pw1
 stty echo # activate echo
 printf "\n"

 printf "Input password for device again:"
 stty -echo # deactivate echo, password is not shown
 read pw2
 stty echo # activate echo
 printf "\n"

 #printf "pw1: ${pw1}\n"
 #printf "pw1: ${pw2}\n"

  if [ "$pw1" = "$pw2" ]; then
   printf "Congrat: both passwords are same\n"
   pwcheck="true"

   # overwrite password string with NULL
   pw2=""

   #printf "Check if pw1 is empty: ${pw1}\n" #debug
   #printf "Check if pw2 is empty: ${pw2}\n" #debug

  else
   printf "Try again: both passwords are NOT same\n\n"

  fi

done

### set password for os user <v4l>
printf "Set password for user <v4l>\n"
sudo passwd $name <<- !
$pw1
$pw1
!

### set password for os user <root>
printf "\nSet password for user <root>\n"
sudo passwd root <<- !
$pw1
$pw1
!

### set password in db.h source file
printf "\nSet password for user <v4l>\n"
sudo sed -i "/C_db_dataBasePassword/c\#define\tC_db_dataBasePassword\t\t\"${pw1}\"" $srcdir/db/db.h

### set password for x11vnc
printf "\nSet password for x11vnc\n"
sudo x11vnc -storepasswd $homedir/.vnc/passwd <<- !
$pw1
$pw1
y
!

### show MariaDB version
printf "\nShow MariaDB version:\n"
mysql -V --database=testBase --user=trimada --password=Master-5610
 # different way ith more details
 #mysql --database=testBase --user=trimada --password=Master-5610 --execute="SHOW VARIABLES LIKE \"%version%\";"

### show existing accounts in MariaDB
printf "\nShow all accounts:\n"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SELECT host, user, password FROM mysql.user;"

printf "\nSet password for MariaDB user <trimada>\n"
### set password for MariaDB user <v4l> -> not neccessary because when creating DB with new build binarie "dbcreate" the new password in db.h is used
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SET PASSWORD FOR \"trimada\"@localhost = PASSWORD(\"${pw1}\");"
 # different way
 #mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE mysql.user SET Password = PASSWORD(\"${pw1}\") WHERE User = \"trimada\";"

printf "\nSet password for MariaDB user <root>\n"
mysql --database=testBase --user=root --password=Master-5610 --execute="SET PASSWORD FOR \"root\"@localhost = PASSWORD(\"${pw1}\");"


### overwrite password string with NULL
pw1=""


## firewall
fwyes=""
printf "\n\nActivate standard firewall rules?\n y = yes: "
read fwyes
printf "\n"

#printf "fwyes: ${fwyes}\n"

fwp22=""
fwp5900=""
fwp7575=""
fwp10000=""
fwp10022=""
fwp17005=""

if [ "$fwyes" = "y" ]; then

### Enable firewall
printf "\nEnable firewall\n"
sudo ufw enable <<- !
y
!

  ### add tcp ports to rule allow

   #### 22
   printf "\n\nWanna activate incoming port 22 for ssh server?\n y = yes: "
   read fwp22
   #printf "\n"

   if [ "$fwp22" = "y" ]; then

    printf "Enabled port 22\n"
    sudo ufw allow 22/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi

   #### 5900
   printf "\n\nWanna activate incoming port 5900 for vnc server?\n y = yes: "
   read fwp5900
   #rintf "\n"

   if [ "$fwp5900" = "y" ]; then

    printf "Enabled port 5900\n"
    sudo ufw allow 5900/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi

   #### 7575
   printf "\n\nWanna activate incoming port 7575 for virtualhere server?\n y = yes: "
   read fwp7575
   #printf "\n"

   if [ "$fwp7575" = "y" ]; then

    printf "Enabled port 7575\n"
    sudo ufw allow 7575/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi

   #### 10000
   printf "\n\nWanna activate incoming port 10000 for v4l server?\n y = yes: "
   read fwp10000
   #printf "\n"

   if [ "$fwp10000" = "y" ]; then

    printf "Enabled port 10000\n"
    sudo ufw allow 10000/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi


   #### 10022
   printf "\n\nWanna activate incoming port 10022 for ssh server?\n y = yes: "
   read fwp10022
   #printf "\n"

   if [ "$fwp10022" = "y" ]; then

    printf "Enabled port 10022\n"
    sudo ufw allow 10022/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi

   #### 17005
   printf "\n\nWanna activate incoming port 17005 for ssh sever with techdock m2m-cloud?\n y = yes: "
   read fwp17005
   #printf "\n"

   if [ "$fwp17005" = "y" ]; then

    printf "Enabled port 17005\n"
    sudo ufw allow 17005/tcp > /dev/null 2>&1 # redirect output for make cmd silent

   fi

  ### activate rules
  printf "\nActivate firewall rules\n"
  sudo ufw default allow outgoing > /dev/null 2>&1 # redirect output for make cmd silent
  sudo ufw default deny incoming > /dev/null 2>&1 # redirect output for make cmd silent

  ### state firewall
  printf "\nShow status firewall\n"
  sudo ufw status verbose

else

 printf "\nFor config firewall individuell use command ufw help\n"

fi


## SSH
sshyes=""

printf "\nSSH server port: be carefully and do not lock out yourself, becasue firefall rules above ist activ!"
printf "\nFor add manual ports check out /etc/ssh/sshd_config"
printf "\nNew rules are avtive after rebooting\n\n"

#printf "fwp22: ${fwp22}\n"

 if [ "$fwp22" = "y" ]; then

  printf "Port 22 is allready activ\n\n"

 else

  ### replace full line which contains Port 22 in file sshd_config
  sudo sed -i "/Port 22/c\#Port 22" /etc/ssh/sshd_config
  printf "Deactivte SSH server port 22 in /etc/ssh/sshd_config\n\n"

 fi


 if [ "$fwp10022" = "y" ]; then
  printf "Port 10022 is allready activ\n\n"

  ### replace full line which contains Port 10022 in file sshd_config
  sudo sed -i "/#Port 10022/c\Port 10022" /etc/ssh/sshd_config
  printf "Activat SSH server port 10022 in /etc/ssh/sshd_config\n\n"

 fi

 if [ "$fwp17005" = "y" ]; then
  printf "Port 17005 is allready activ\n\n"

  ### replace full line which contains Port 10022 in file sshd_config
  sudo sed -i "/#Port 17005/c\Port 17005" /etc/ssh/sshd_config
  printf "Activat SSH server port 17005 in /etc/ssh/sshd_config\n\n"

 fi


## VirtualHere Server
wwwaccess=""
vhenable=""

printf "\nIs internet access available at this device?\n y = yes: "
read wwwaccess
#printf "\n"

#printf "wwwaccess: ${wwwaccess}\n"

if [ "$wwwaccess" = "y" ]; then

 printf "\nEnable VirtualHere server service?\n y = yes, else = no : "
 read vhenable
 #printf "\n"

 #printf "vhenable: ${vhenable}\n"

 if [ "$vhenable" = "y" ]; then

  printf "\nInstall VirtualHere server and enable service\n"

  ### change in script disable to enable
  sudo sed -i "/  systemctl disable virtualhere.service/c\  systemctl enable virtualhere.service" $homedir/projects/vhs/install_server

  ### install with script
  sudo sh $homedir/projects/vhs/install_server

  ### change back in script enable to disable
  sudo sed -i "/  systemctl enable virtualhere.service/c\  systemctl disable virtualhere.service" $homedir/projects/vhs/install_server

 else

  printf "\nInstall VirtualHere server and disable service\n"
  sudo sh $homedir/projects/vhs/install_server

 fi

fi


## load  systemctl list new for make sure services are started when rebooting
printf "Reloaded systemctl\n"
sudo systemctl daemon-reload


# end skript
printf "### end ${filename}\n\n"
