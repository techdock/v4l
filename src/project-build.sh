#! /bin/bash

# file          project-start.sh
# brief         script for project vpn4log
# details       starting all running services from project
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       07.04.2023      mimi    - creating
#

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)
user="v4l"
cuser=$(whoami)

printf "\n### Running ${filename} to start project ${project} services as current user ${cuser} and sudoer ${user}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Project directory is:    ${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# stop all project services in case they are still running

## stoppen aller v4l Dienste
sudo systemctl stop configeths
sudo systemctl stop configntp
sudo systemctl stop tcpss
sudo systemctl stop usbevent
sudo systemctl stop shutdown

## killen aller v4l prozess
sudo pkill configeths
sudo pkill configntp
sudo pkill tcpss
sudo pkill usbevent
sudo pkill shutdown

## entfernen aller v4l dienst aus der systemctl liste für autostart
sudo systemctl disable configeths
sudo systemctl disable configntp
sudo systemctl disable tcpss
sudo systemctl disable usbevent
sudo systemctl disable shutdown

## neu laden der systemctl liste für autostart
sudo systemctl daemon-reload


# cleaning project folder

## remove all project files
#cd $bindir
sudo rm $bindir/11007-PA-default.csv
sudo rm $bindir/11007-TP-default.csv
sudo rm $bindir/configeths
sudo rm $bindir/configntp
sudo rm $bindir/conv2utf8
sudo rm $bindir/dbcreate
sudo rm $bindir/dbdelete
sudo rm $bindir/dhcpcd.conf
sudo rm $bindir/interfaces-template
sudo rm $bindir/rt_tables
sudo rm $bindir/shutdown
sudo rm $bindir/tcpss
sudo rm $bindir/timesyncd-template.conf
sudo rm $bindir/usbevent
sudo rm $bindir/usbhello


# create servie files

## create configeths.service file
filedir=$srcdir/tools/configeths/configeths.service
printf "Create configeths.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
[Unit]
Description=Start config eth0 from $project project

Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
ExecStartPre=-/bin/sleep 1
ExecStart=$instdir/configeths
Restart=on-failure
RestartSec=2
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

## create configntp.service file
filedir=$srcdir/tools/configntp/configntp.service
printf "Create configntp.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
[Unit]
Description=Start config ntp from $project project

Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
ExecStartPre=/bin/sleep 10
ExecStart=$instdir/configntp
Restart=on-failure
RestartSec=4
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

## create shutdown.service file
filedir=$srcdir/tools/shutdown/shutdown.service
printf "Create shutdown.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
[Unit]
Description=Start shutdown from $project project

Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
ExecStartPre=/bin/sleep 10
ExecStart=$instdir/shutdown
Restart=on-failure
RestartSec=4
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

## create usbevent.service file
filedir=$srcdir/tools/usbevent/usbevent.service
printf "Create usbevent.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
[Unit]
Description=Start usbevent from $project project

Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
User=$user
ExecStart=$instdir/usbevent
StandardOutput=inherit
StandardError=inherit
Restart=on-failure
RestartSec=10
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

## create tcpss.service file
filedir=$srcdir/tcpss/tcpss.service
printf "Create tcpss.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
[Unit]
Description=Start tcpss from $project project

Wants=network.target
After=syslog.target network-online.target

[Service]
Type=simple
User=$user
ExecStartPre=/bin/sleep 10
ExecStart=$instdir/tcpss-start.sh
StandardOutput=inherit
StandardError=inherit
Restart=on-failure
RestartSec=10
KillMode=process

[Install]
WantedBy=multi-user.target
EOF


# creating rueles.d file

## trigger for usbhello
filedir=$srcdir/tools/usbhello/custom_usb.rules
printf "Create tcpss.service for ${project} in ${filedir}\n"
cat > $filedir <<EOF
# file          custom_usb.rules
# brief         detect event "usb storage plug in"
# details       rule to execute usbhello at the event "usb storage plug in"
# date          19.07.2021
# author        Marcel Ming
# copyright     TRiMADA AG, CH-5610 Wohlen
#
# version       19.07.2021      mimi    - creating
#               25.04.2023      mimi    - export file creating to skript "project-build.sh"
#
# info          how to find the possible attributes:
#               1. send CMD 'lsblk' at CLI and copy name, for e.g. 'sda1'
#               2. send CMD udevadm info --attribute-walk --name=/dev/sda1 at CLI
#               3. check attributes in the shown infos to the device 'sda1'

# this file was automatic generated by skript "project-build.sh"

## test other way to execute usbhello at usb event
#ACTION=="add", DRIVERS=="usb-storage", RUN="${instdir}/usbhello"

## origin way from TRIMADA
ACTION=="add", SUBSYSTEM=="usb", ATTRS{bInterfaceClass}=="08", RUN="${instdir}/usbhello"
EOF


# creating bindir if not existing

## create folder /home/<user>/projects/<project>/opt/
sudo mkdir $prodir/opt > /dev/null 2>&1 # redirect output for make cmd silent if directory is allready existing

## create folder /home/<user>/projects/<project>/opt/<project>
sudo mkdir $bindir > /dev/null 2>&1 # redirect output for make cmd silent if directory is allready existing


# creating all sympolic links new

## create new symlinks for bash project.install.sh
#cd $bindir
#ls -la
sudo ln -s -f $srcdir/db/11007-PA-default.csv $bindir/11007-PA-default.csv
sudo ln -s -f $srcdir/db/11007-TP-default.csv $bindir/11007-TP-default.csv
sudo ln -s -f $srcdir/tools/configeths/dhcpcd.conf $bindir/dhcpcd.conf
sudo ln -s -f $srcdir/tools/configeths/interfaces-template $bindir/interfaces-template
sudo ln -s -f $srcdir/tools/configeths/rt_tables $bindir/rt_tables
sudo ln -s -f $srcdir/tools/configntp/timesyncd-template.conf $bindir/timesyncd-template.conf
#ls -la

## create new symlinks for make configets
#cd $srcdir/tools/configeths/
path=$srcdir/tools/configeths
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
#ls -la

## create new symlinks for make configntp
#cd $srcdir/tools/configntp/
path=$srcdir/tools/configntp
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
#ls -la

## create new symlinks for make conv2utf8
#cd $srcdir/tools/conv2utf8/
path=$srcdir/tools/conv2utf8
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
#ls -la

## create new symlinks for make dbcreate
#cd $srcdir/tools/dbcreate/
path=$srcdir/tools/dbcreate
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
#ls -la

## create new symlinks for make dbdelete
#cd $srcdir/tools/dbdelete/
path=$srcdir/tools/dbdelete
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
#ls -la

## create new symlinks for make usbevent
#cd $srcdir/tools/usbevent/
path=$srcdir/tools/usbevent
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/lib/c4lusb.c $path/c4lusb.c
sudo ln -s -f $srcdir/lib/c4lusb.h $path/c4lusb.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
sudo ln -s -f $srcdir/lib/mail.c $path/mail.c
sudo ln -s -f $srcdir/lib/mail.h $path/mail.h
sudo ln -s -f $srcdir/lib/ptkC4l.c $path/ptkC4l.c
sudo ln -s -f $srcdir/lib/ptkC4l.h $path/ptkC4l.h
#ls -la

## create new symlinks for make usbhello
#cd $srcdir/tools/usbhello/
path=$srcdir/tools/usbhello
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
#ls -la

## create new symlinks for make tcpss
#cd $srcdir/tcpss
path=$srcdir/tcpss
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
sudo ln -s -f $srcdir/lib/ptkC4l.c $path/ptkC4l.c
sudo ln -s -f $srcdir/lib/ptkC4l.h $path/ptkC4l.h
#ls -la

## create new symlinks for make shutdown
#cd $srcdir/tools/shutdown/
path=$srcdir/tools/shutdown
#ls -la
sudo ln -s -f $srcdir/lib/buildDate.h $path/buildDate.h
sudo ln -s -f $srcdir/db/csv.c $path/csv.c
sudo ln -s -f $srcdir/db/csv.h $path/csv.h
sudo ln -s -f $srcdir/lib/csw.h $path/csw.h
sudo ln -s -f $srcdir/db/db.c $path/db.c
sudo ln -s -f $srcdir/db/db.h $path/db.h
sudo ln -s -f $srcdir/lib/mail.c $path/mail.c
sudo ln -s -f $srcdir/lib/mail.h $path/mail.h
#ls -la


# build lib's new

##  build all lib's
cd $srcdir/tools/configeths/
sudo make
cd $srcdir/tools/configntp/
sudo make 
cd $srcdir/tools/conv2utf8/
sudo make
cd $srcdir/tools/dbcreate/
sudo make
cd $srcdir/tools/dbdelete/
sudo make
cd $srcdir/tools/usbevent/
sudo make
cd $srcdir/tools/usbhello/
sudo make
cd $srcdir/tcpss/
sudo make
cd $srcdir/tools/shutdown/
sudo make

## show build libs in bindir
cd $bindir
ls -la


# end skript
printf "### end ${filename}\n\n"
