#! /bin/bash

# file          project-install.sh
# brief         script for project vpn4log
# details       copy all needed project files to /opt/v4l
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       07.04.2023      mimi    - creating
#

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)

printf "\n### Running ${filename} to install project ${project}-files and symlinks\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:	${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:	${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent


# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:	${bindir}\n"

# create bindir
sudo mkdir $instdir > /dev/null 2>&1 # redirect output for make cmd silent if directory allready existing


# copy project files, copy simlinks and create symlinks for services

## usb
printf "\n# handle USB config installation\n"

#echo "copy "$srcdir"/tools/usbevent/usbplugged.sh to ${instdir}/usbplugged.sh"
#sudo cp -f $srcdir/tools/usbevent/usbplugged.sh $instdir/usbplugged.sh

echo "copy ${bindir}/usbhello to ${instdir}/usbhello"
sudo cp -f $bindir/usbhello $instdir/usbhello

echo "create softlink /etc/udev/rules.d/custom_usb.rules"
sudo ln -s -f $srcdir/tools/usbhello/custom_usb.rules /etc/udev/rules.d/custom_usb.rules

echo "copy ${bindir}/usbevent to ${instdir}/usbevent"
sudo cp -f $bindir/usbevent $instdir/usbevent

echo "create softlink /etc/systemd/system/usbevent.service and enable service"
sudo ln -s -f $srcdir/tools/usbevent/usbevent.service /etc/systemd/system/usbevent.service
sudo systemctl enable usbevent.service

## shutdown
printf "\n# handel FC99 restart installation\n"

echo "copy ${bindi}/shutdown to ${instdir}/shutdown"
sudo cp -f $bindir/shutdown $instdir/shutdown

echo "create softlink /etc/systemd/system/shutdown.service and enable service"
sudo ln -s -f $srcdir/tools/shutdown/shutdown.service /etc/systemd/system/shutdown.service
sudo systemctl enable shutdown.service

## eth
printf "\n# handle config of eth installation\n"

echo "copy ${bindir}/configeths to ${instdir}/configeths"
sudo cp -f $bindir/configeths $instdir/configeths

echo "copy ${bindir}/interfaces-template to ${instdir}/interfaces-template"
sudo cp -f $bindir/interfaces-template $instdir/interfaces-template

echo "copy ${bindir}/dhcpcd.conf to /etc/dhcpcd.conf"
sudo cp -f $bindir/dhcpcd.conf /etc/dhcpcd.conf

echo "copy ${bindir}/rt_tables to /etc/iproute2/rt_tables"
sudo cp -f $bindir/rt_tables /etc/iproute2/rt_tables

echo "create softlink /etc/systemd/system/configeths.service and enable service"
sudo ln -s -f $srcdir/tools/configeths/configeths.service /etc/systemd/system/configeths.service
sudo systemctl enable configeths.service

## ntp
printf "\n# handle config of ntp installation\n"

echo "copy ${bindir}/configntp to ${instdir}/configntp"
sudo cp -f $bindir/configntp $instdir/configntp

echo "copy ${bindir}/interfaces-template to ${instdir}/interfaces-template"
sudo cp -f $bindir/timesyncd-template.conf $instdir/timesyncd-template.conf

echo "create softlink /etc/systemd/system/configntp.service and enable service"
sudo ln -s -f $srcdir/tools/configntp/configntp.service /etc/systemd/system/configntp.service
sudo systemctl enable configntp.service

## database
printf "\n# handle database installation\n"

echo "copy ${bindir}/dbcreate to ${instdir}/create"
sudo cp -f $bindir/dbcreate $instdir/dbcreate

echo "copy ${bindir}/dbdelete to ${instdir}/dbdelete"
sudo cp -f $bindir/dbdelete $instdir/dbdelete

echo "copy ${bindir}/11007-PA-default.csv to ${instdir}/11007-PA-default.csv"
sudo cp -f $bindir/11007-PA-default.csv $instdir/11007-PA-default.csv

echo "copy ${bindir}/11007-TP-default.csv to ${instdir}/11007-TP-default.csv"
sudo cp -f $bindir/11007-TP-default.csv $instdir/11007-TP-default.csv

## socket server
printf "\n# handle tcp socket server installation\n"

echo "copy ${bindir}/tcpss to ${instdir}/tcpss"
sudo cp -f $bindir/tcpss $instdir/tcpss

echo "copy ${srcdir}/tcpss-start.sh to ${instdir}/tcpss-start.sh"
sudo cp -f $srcdir/tcpss-start.sh $instdir/tcpss-start.sh

echo "create softlink /etc/systemd/system/tcpss.service and enable service"
sudo ln -s -f $srcdir/tcpss/tcpss.service /etc/systemd/system/tcpss.service
sudo systemctl enable tcpss.service


# build db new with binaries in instdir

## delete first db and then create again the db
sudo $instdir/dbdelete
sudo $instdir/dbcreate


# end skript
printf "### end ${filename}\n\n"
