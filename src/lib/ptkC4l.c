/**************************************************************************//**
 *
 *	\file		ptkC4l.c
 *
 *	\brief		stellt Funktionen für das free2sample Protokoll zur Verfügung.
 *
 *	\details	-
 *
 *	\date		18.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	18.03.21 mm
 *					- Erstellt.
 * 	\version	19.07.21 ch
 * 					- Alle Funktionen enthalten ausser SC
 * 					- Write Protection Fehlermeldung ergänzt
 * 	\version	19.08.21 ch
 * 					- Signalisierung, dass Netwerkeinstellungen geändert haben via
 * 					  FLAG
 * 					- Unterbindung, dass NULL als Zahlenwert geschrieben werden 
 * 					  kann. 
 * 	\version	23.08.21 ch
 * 					- Signalisierung, dass NTP-Einstellungen geändert haben via
 * 					  FLAG
 * 					- SC-Kommando via timedatectl
 * 	\version	23.08.21 mm
 * 					- Falls der gewünschte Eintrag des Ausgangs in der Datenbank
 *					  beim Aufruf der Funktionen ptkC4l_evaluateDO und ptkC4l_evaluateAO
 *					  nicht mit db_rowUpdate gesetzt werden kann, wird db_rowAdd
 *					  augerufen.
 * 	\version	03.09.21 mm
 * 					- Auswerung FC99 implementiert.
 * 	\version	25.10.21 mm
 * 					- in ptkC4l_evaluateDL wird die id der Anfrage (Historyposition)
 *					  und nicht mehr die id des Datensatzes zurückgegeben.
 *					- in ptkC4l_evaluateEV ebenso.
 * 	\version	19.11.21 mm
 * 					- Fehlermeldungen in ptkC4l_evaluatePA korrigiert.
 *	\version	25.11.21 mm
 *					- Zugriffsrechte der einzelnen eth's implementiert.
 *	\version	29.11.21 mm
 *					- Datenbankzugriffe reduziert.
 *	\version	07.12.21 mm
 *					- Funktion ptkC4l_getNumber überarbeitet. Rückgabewert bei
 *					  einem Fehler ist jetzt -1.
 *					- Fehlermeldungen bei allen Tabellenabfragen ergänzt.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "db.h"
#include "ptkC4l.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
int ptkC4l_serviceModeActiv = 0;


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		ptkC4l_getNumber
 *
 *  \details	gibt die Nummer im Command String als Integer zurück. Die Suche
 *				startet bei pos und endet bei den Zeichen '.' oder '\r'.
 *
 *	\param		**pCmdStr	Pointer auf den Pointer des Commando String
 *
 *	\return		Nummer oder -1 wenn keine Nummer gefunden wird.
 *
 ******************************************************************************/
static int ptkC4l_getNumber (char **pCmdStr)
{
	char numStr [C_ptkC4l_digitCount + 1] = "";
	int	 numPos = 0;
	int	 retValue = -1;

	while (isdigit (**pCmdStr))
	{
		numStr [numPos++] = *(*pCmdStr)++;
	}
	numStr [numPos] = '\0';

	if (numPos > 0)
	{
		retValue = atoi (numStr);
	}

	return (retValue);
} /* ptkC4l_getNumber */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluatePA
 *
 *  \details	evaluiert den PA Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu PA geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die Parameter (ausser PA7) können nur geschrieben werden wenn
 *				ptkC4l_serviceModeActiv 1 ist.
 *
 ******************************************************************************/
static void ptkC4l_evaluatePA (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_pa			  dbPA;
	te_db_tablePermission tablePermission;


	dbPA.id = ptkC4l_getNumber (&pCmdStr);

	if (dbPA.id >= 0)
	{
		// bestehende Werte aus Datenbank lesen (für read oder rangeCheck bei write)
		if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
		{
			switch (*pCmdStr++)
			{
				case '.':
					// PA schreiben
					if (ptkC4l_replaceChar1withChar2 (pCmdStr, '\r', '\0') == 0)
					{
						tablePermission = db_tablePermission (pConnect, E_db_tableTypePA, ethNumber);

						if (tablePermission == E_db_tablePermissionReadWrite)
						{
							if ((ptkC4l_serviceModeActiv == 1) || 
								(dbPA.id == E_db_PAoAlgWinterSummerTime))
							{
								strncpy(dbPA.strValue, pCmdStr, C_db_dataStringSize);
								//db_rowDebug (E_db_tableTypePA, &dbPA);
								switch (db_PArowRangeCheck (&dbPA))
								{
									case E_db_PArangeCheckOk:
										if (db_rowUpdate (pConnect, E_db_tableTypePA, &dbPA) == 0)
										{
											snprintf (pReplyStr, C_db_dataStringSize, "@PA%d.%s\r\n",
													  dbPA.id, dbPA.strValue);
										} // if (db_rowUpdate (pConnect, E_db_tableTypePA, &dbPA) == 0)
										else
										{
											snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
													  dbPA.id, C_ptkC4l_errMsgDbWrite); 
										} // else if (db_rowUpdate (pConnect, E_db_tableTypePA, &dbPA) == 0)
										break;
									default:
										snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
													  dbPA.id, C_ptkC4l_errMsgValueInvalid);
										break;
								} // switch (dbPA_rowRangeCheck (pConnect, pDbPA))
							} // if((ptkC4l_serviceModeActiv == 1) ||...
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
										  dbPA.id, C_ptkC4l_errMsgValueProtected);
							} // else if((ptkC4l_serviceModeActiv == 1) ||...
						}
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
									  dbPA.id, C_ptkC4l_errMsgAccessDenied);
						} // else if (tablePermission == E_db_tablePermissionReadWrite)
					} // if (ptkC4l_replaceBSrToBS0 (pCmdStr) == 0)
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
									  dbPA.id, C_ptkC4l_errMsgCmdInvalid);
					} // else: if (ptkC4l_replaceBSrToBS0 (pCmdStr) == 0)
					break;

				case '\r':
					// PA lesen
					if (*pCmdStr == '\n')
					{
						tablePermission = db_tablePermission (pConnect, E_db_tableTypePA, ethNumber);

						if ((tablePermission == E_db_tablePermissionRead) ||
							(tablePermission == E_db_tablePermissionReadWrite))
						{
							// vorgängig gelesener Wert zurückgeben
							snprintf (pReplyStr, C_db_dataStringSize, "@PA%d.%s\r\n",
									  dbPA.id, dbPA.strValue);
						}
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
									  dbPA.id, C_ptkC4l_errMsgAccessDenied);
						} // else if ((tablePermission == E_db_tablePermissionReadWrite) || ...
					} // if (pCmdStr [pos] == '\n')
					break;
				default:
					break;
			} // switch (*pCmdStr++)
		} // if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
		else
		{
			snprintf (pReplyStr, C_db_dataStringSize, "@PA%d%s\r\n",
					  dbPA.id, C_ptkC4l_errMsgDbRead);
		} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
	} // if (dbPA.id >= 0)
} /* ptkC4l_evaluatePA */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateTP
 *
 *  \details	evaluiert den TP Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu TP geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 ******************************************************************************/
static void ptkC4l_evaluateTP (ts_db_connect *pConnect,
                               char			 *pCmdStr,
                               char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_tp			  dbTP;
	te_db_tablePermission tablePermission;

	
	dbTP.id = ptkC4l_getNumber (&pCmdStr);

	if (dbTP.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '.':
				// TP schreiben
				if (ptkC4l_replaceChar1withChar2 (pCmdStr, '\r', '\0') == 0)
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeTP, ethNumber);

					if (tablePermission == E_db_tablePermissionReadWrite)
					{
						strncpy(dbTP.strText, pCmdStr, C_db_dataStringSize);
						if (db_rowUpdate (pConnect, E_db_tableTypeTP, &dbTP) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@TP%d.%s\r\n",
									  dbTP.id, dbTP.strText);
						} // if (db_rowUpdate (pConnect, E_db_tableTypeTP, &dbTP) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@TP%d%s\r\n",
									  dbTP.id, C_ptkC4l_errMsgDbWrite); 
						} // else if (db_rowUpdate (pConnect, E_db_tableTypeTP, &dbTP) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@TP%d%s\r\n",
								  dbTP.id, C_ptkC4l_errMsgAccessDenied);
					} // else if (tablePermission == E_db_tablePermissionReadWrite)
				} // if (ptkC4l_replaceBSrToBS0 (pCmdStr) == 0)
				break;
			case '\r':
				if (*pCmdStr == '\n')
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeTP, ethNumber);

					if ((tablePermission == E_db_tablePermissionRead) ||
						(tablePermission == E_db_tablePermissionReadWrite))
					{
						// TP lesen
						if (db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@TP%d.%s\r\n",
									  dbTP.id, dbTP.strText);
						} // if (db_rowGet (pConnect, E_db_tableTypeTP, &dbTP) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@TP%d%s\r\n",
									  dbTP.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeTP, &dbTP) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@TP%d%s\r\n",
								  dbTP.id, C_ptkC4l_errMsgAccessDenied);
					} // else if ((tablePermission == E_db_tablePermissionReadWrite) || ...
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbTP.id > 0)
} /* ptkC4l_evaluateTP */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateVA
 *
 *  \details	evaluiert den VA Command String. 
 * 
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu VA geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die Variablen können nur gelesen werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateVA (ts_db_connect *pConnect,
                               char			 *pCmdStr,
                               char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_va	dbVA;

	
	dbVA.id = ptkC4l_getNumber (&pCmdStr);

	if (dbVA.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '\r':
				if (*pCmdStr == '\n')
				{
					if (db_tablePermission (pConnect, E_db_tableTypeVA, ethNumber) == E_db_tablePermissionRead)
					{
						// VA lesen
						if (db_rowGet (pConnect, E_db_tableTypeVA, E_db_listTypeNone, 0, &dbVA) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@VA%d.%s\r\n",
									  dbVA.id, dbVA.strDescription);
						} // if (db_rowGet (pConnect, E_db_tableTypeVA, &dbVA) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@VA%d%s\r\n",
									  dbVA.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeVA, &dbVA) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@VA%d%s\r\n",
								  dbVA.id, C_ptkC4l_errMsgAccessDenied);
					} // if ((db_tablePermission (pConnect, E_db_tableTypeVA, ethNumber) == E_db_tablePermissionRead)
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbVA.id > 0)
} /* ptkC4l_evaluateVA */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateDI
 *
 *  \details	evaluiert den DI Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu DI geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die digitalen Eingänge können nur gelesen werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateDI (ts_db_connect *pConnect,
                               char			 *pCmdStr,
                               char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_di	dbDI;
	
	dbDI.id = ptkC4l_getNumber (&pCmdStr);
	
	if (dbDI.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '\r':
				if (*pCmdStr == '\n')
				{
					if (db_tablePermission (pConnect, E_db_tableTypeDI, ethNumber) == E_db_tablePermissionRead)
					{
						// DI lesen
						if (db_rowGet (pConnect, E_db_tableTypeDI, E_db_listTypeDI, dbDI.id, &dbDI) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@DI%d.%s\r\n",
									  dbDI.id, dbDI.strValue);
						} // if (db_rowGet (pConnect, E_db_tableTypeDI, &dbDI) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@DI%d%s\r\n",
									  dbDI.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeDI, E_db_listTypeDI, dbDI.id, &dbDI) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@DI%d%s\r\n",
								  dbDI.id, C_ptkC4l_errMsgAccessDenied);
					} // if ((db_tablePermission (pConnect, E_db_tableTypeDI, ethNumber) == E_db_tablePermissionRead)
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbDI.id > 0)
} /* ptkC4l_evaluateDI */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateDO
 *
 *  \details	evaluiert den DO Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu DO geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die digitalen Ausgänge können nur geschrieben werden wenn
 *				ptkC4l_serviceModeActiv 1 ist.
 *
 ******************************************************************************/
static void ptkC4l_evaluateDO (ts_db_connect *pConnect,
                               char			 *pCmdStr,
                               char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_do			  dbDO;
	te_db_tablePermission tablePermission;


	dbDO.id = ptkC4l_getNumber (&pCmdStr);

	if (dbDO.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '.':
				// DO schreiben
				if (ptkC4l_replaceChar1withChar2 (pCmdStr, '\r', '\0') == 0)
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeDO, ethNumber);

					if (tablePermission == E_db_tablePermissionReadWrite)
					{
						if(ptkC4l_serviceModeActiv == 1)
						{
							strncpy(dbDO.strValue, pCmdStr, C_db_dataStringSize);
							
							if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO) == 0)
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@DO%d.%s\r\n",
										  dbDO.id, dbDO.strValue);
							} // if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO) == 0)
							else
							{
								// Zeile existiert nicht -> add
								if ((dbDO.id > 0) && (dbDO.id <= C_db_maxRowDo) &&
									(db_rowAdd (pConnect, E_db_tableTypeDO, E_db_listTypeNone, dbDO.id, &dbDO) == 0))
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DO%d.%s\r\n",
											  dbDO.id, dbDO.strValue);
								} // if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DO%d%s\r\n",
											  dbDO.id, C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO) == 0)
							} // else if (db_rowUpdate (pConnect, E_db_tableTypeDO, &dbDO) == 0)
						} // if ((ptkC4l_replaceBSrToBS0 (pCmdStr) == 0) ...
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@DO%d%s\r\n",
									  dbDO.id, C_ptkC4l_errMsgValueProtected);
						} // else if(ptkC4l_serviceModeActiv == 1)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@DO%d%s\r\n",
								  dbDO.id, C_ptkC4l_errMsgAccessDenied);
					} // else if (tablePermission == E_db_tablePermissionReadWrite)
				} // if ((ptkC4l_replaceBSrToBS0 (pCmdStr) == 0) 
				break;
			case '\r':
				if (*pCmdStr == '\n')
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeDO, ethNumber);

					if ((tablePermission == E_db_tablePermissionRead) ||
						(tablePermission == E_db_tablePermissionReadWrite))
					{
						// DO lesen
						if (db_rowGet (pConnect, E_db_tableTypeDO, E_db_listTypeDO, dbDO.id, &dbDO) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@DO%d.%s\r\n",
									  dbDO.id, dbDO.strValue);
						} // if (db_rowGet (pConnect, E_db_tableTypeTP, &dbTP) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@DO%d%s\r\n",
									  dbDO.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeDO, E_db_listTypeDO, dbDO.id, &dbDO) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@DO%d%s\r\n",
								  dbDO.id, C_ptkC4l_errMsgAccessDenied);
					} // else if ((tablePermission == E_db_tablePermissionReadWrite) || ...
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbDO.id > 0)
} /* ptkC4l_evaluateDO  */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateDL
 *
 *  \details	evaluiert den DL Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu DL geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die Datenpunktlisten können nur gelesen werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateDL (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_dl	dbDL;
	int			channel;
	int			idinCmd;


	switch (*pCmdStr++)
	{
		case 'D':
			switch (*pCmdStr++)
			{
				case 'I':
					channel = ptkC4l_getNumber (&pCmdStr);

					if (channel > 0)
					{
						if (*pCmdStr++ == '.')
						{
							idinCmd = ptkC4l_getNumber (&pCmdStr);
							dbDL.id = idinCmd - 1;

							if ((*pCmdStr == '\r') && (*(pCmdStr + 1) == '\n'))
							{
								if (db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
								{
									// DLDI lesen
									if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeDI, channel, &dbDL) == 0)
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLDI%d.%d.%s.%s\r\n",
										channel, idinCmd, dbDL.strValue, dbDL.strDateTime);
									}
									else
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLDI%d.%d%s\r\n",
												  channel, idinCmd, C_ptkC4l_errMsgDbRead);
									} // else if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeDI, channel, &dbDL) == 0)
								}
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DLDI%d.%d%s\r\n",
											  channel, idinCmd, C_ptkC4l_errMsgAccessDenied);
								} // else if ((db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
							} // if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
						} // if (*pCmdStr++ == '.')
					} // if (channel > 0)
					break;

				case 'O':
					channel = ptkC4l_getNumber (&pCmdStr);

					if (channel > 0)
					{
						if (*pCmdStr++ == '.')
						{
							idinCmd = ptkC4l_getNumber (&pCmdStr);
							dbDL.id = idinCmd - 1;

							if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
							{
								if (db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
								{
									// DLDO lesen
									if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeDO, channel, &dbDL) == 0)
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLDO%d.%d.%s.%s\r\n",
										channel, idinCmd, dbDL.strValue, dbDL.strDateTime);
									} // if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeDO, channel, &dbDL) == 0)
									else
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLDO%d.%d%s\r\n",
										channel, idinCmd, C_ptkC4l_errMsgDbRead);
									} // else if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeDO, channel, &dbDL) == 0)
								}
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DLDO%d.%d%s\r\n",
											  channel, idinCmd, C_ptkC4l_errMsgAccessDenied);
								} // else if ((db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
							} // if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
						} // if (*pCmdStr++ == '.')
					} // if (channel > 0)
					break;

				default:
					break;
			} // switch (*pCmdStr++)
			break;

		case 'A':
			switch (*pCmdStr++)
			{
				case 'I':
					channel = ptkC4l_getNumber (&pCmdStr);

					if (channel > 0)
					{
						if (*pCmdStr++ == '.')
						{
							idinCmd = ptkC4l_getNumber (&pCmdStr);
							dbDL.id = idinCmd - 1;

							if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
							{
								if (db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
								{
									// DLAI lesen
									if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAI, channel, &dbDL) == 0)
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLAI%d.%d.%s.%s\r\n",
										channel, idinCmd, dbDL.strValue, dbDL.strDateTime);
									} // if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAI, channel, &dbDL) == 0)
									else
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLAI%d.%d%s\r\n",
										channel, idinCmd, C_ptkC4l_errMsgDbRead);
									} // else if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAI, channel, &dbDL) == 0)
								}
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DLAI%d.%d%s\r\n",
											  channel, idinCmd, C_ptkC4l_errMsgAccessDenied);
								} // else if ((db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
							} // if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
						} // if (*pCmdStr++ == '.')
					} // if (channel > 0)
					break;

				case 'O':
					channel = ptkC4l_getNumber (&pCmdStr);

					if (channel > 0)
					{
						if (*pCmdStr++ == '.')
						{
							idinCmd = ptkC4l_getNumber (&pCmdStr);
							dbDL.id = idinCmd - 1;

							if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
							{
								if (db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
								{
									// DLAO lesen
									if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAO, channel, &dbDL) == 0)
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLAO%d.%d.%s.%s\r\n",
										channel, idinCmd, dbDL.strValue, dbDL.strDateTime);
									} // if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAO, channel, &dbDL) == 0)
									else
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLAO%d.%d%s\r\n",
										channel, idinCmd, C_ptkC4l_errMsgDbRead);
									} // else if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeAO, channel, &dbDL) == 0)
								}
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DLAO%d.%d%s\r\n",
											  channel, idinCmd, C_ptkC4l_errMsgAccessDenied);
								} // else if ((db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
							} // if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
						} // if (*pCmdStr++ == '.')
					} // if (channel > 0)
					break;

				default:
					break;
			} // switch (*pCmdStr++)
			break;
		case 'V':
			switch (*pCmdStr++)
			{
				case 'A':
					channel = ptkC4l_getNumber (&pCmdStr);

					if (channel > 0)
					{
						if (*pCmdStr++ == '.')
						{
							idinCmd = ptkC4l_getNumber (&pCmdStr);
							dbDL.id = idinCmd - 1;

							if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
							{
								if (db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
								{
									// DLVA lesen
									if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeVA, channel, &dbDL) == 0)
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLVA%d.%d.%s.%s\r\n",
										channel, idinCmd, dbDL.strValue, dbDL.strDateTime);
									} // if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeVA, channel, &dbDL) == 0)
									else
									{
										snprintf (pReplyStr, C_db_dataStringSize, "@DLVA%d.%d%s\r\n",
										channel, idinCmd, C_ptkC4l_errMsgDbRead);
									} // else if (db_rowGetSorted (pConnect, E_db_tableTypeDL, E_db_listTypeVA, channel, &dbDL) == 0)
								}
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@DLVA%d.%d%s\r\n",
											  channel, idinCmd, C_ptkC4l_errMsgAccessDenied);
								} // else if ((db_tablePermission (pConnect, E_db_tableTypeDL, ethNumber) == E_db_tablePermissionRead)
							} // if ((*pCmdStr == '\r') && (*(pCmdStr+1) == '\n'))
						} // if (*pCmdStr++ == '.')
					} // if (channel > 0)
					break;

				default:
					break;
			} // switch (*pCmdStr++)
			break;
		default:
			break;
	} /* switch (*pCmdStr++) */
} /* ptkC4l_evaluateDL */ 


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateAI
 *
 *  \details	evaluiert den AI Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu AI geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die analogen Eingänge können nur gelesen werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateAI (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_ai	dbAI;
	
	dbAI.id = ptkC4l_getNumber (&pCmdStr);

	if (dbAI.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '\r':
				if (*pCmdStr == '\n')
				{
					if (db_tablePermission (pConnect, E_db_tableTypeAI, ethNumber) == E_db_tablePermissionRead)
					{
						// AI lesen
						if (db_rowGet (pConnect, E_db_tableTypeAI, E_db_listTypeAI, dbAI.id, &dbAI) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@AI%d.%s\r\n",
									  dbAI.id, dbAI.strValue);
						} // if (db_rowGet (pConnect, E_db_tableTypeAI, E_db_listTypeAI, dbAI.id, &dbAI) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@AI%d%s\r\n",
									  dbAI.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeAI, E_db_listTypeAI, dbAI.id, &dbAI) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@AI%d%s\r\n",
								  dbAI.id, C_ptkC4l_errMsgAccessDenied);
					} // else if ((db_tablePermission (pConnect, E_db_tableTypeAI, ethNumber) == E_db_tablePermissionRead)
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbAI.id > 0)
} /* ptkC4l_evaluateAI */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateAO
 *
 *  \details	evaluiert den AO Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu AO geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die digitalen Ausgänge können nur geschrieben werden wenn
 *				ptkC4l_serviceModeActiv 1 ist.
 *
 ******************************************************************************/
static void ptkC4l_evaluateAO (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_ao			  dbAO;
	te_db_tablePermission tablePermission;


	dbAO.id = ptkC4l_getNumber (&pCmdStr);

	if (dbAO.id > 0)
	{
		switch (*pCmdStr++)
		{
			case '.':
				// AO schreiben
				if (ptkC4l_replaceChar1withChar2 (pCmdStr, '\r', '\0') == 0)
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeAO, ethNumber);

					if (tablePermission == E_db_tablePermissionReadWrite)
					{
						if (ptkC4l_serviceModeActiv == 1)
						{
							strncpy(dbAO.strValue, pCmdStr, C_db_dataStringSize);
							if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO) == 0)
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@AO%d.%s\r\n",
										  dbAO.id, dbAO.strValue);
							} // if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO) == 0)
							else
							{
								// Zeile existiert nicht -> add
								if ((dbAO.id > 0) && (dbAO.id <= C_db_maxRowAo) &&
									(db_rowAdd (pConnect, E_db_tableTypeAO, E_db_listTypeNone, dbAO.id, &dbAO) == 0))
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@AO%d.%s\r\n",
											  dbAO.id, dbAO.strValue);
								} // if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@AO%d%s\r\n",
											  dbAO.id, C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO) == 0)
							} // else if (db_rowUpdate (pConnect, E_db_tableTypeAO, &dbAO) == 0)
						} // if (ptkC4l_serviceModeActiv == 1)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@AO%d%s\r\n",
									  dbAO.id, C_ptkC4l_errMsgValueProtected);
						} // else if (ptkC4l_serviceModeActiv == 1)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@AO%d%s\r\n",
								  dbAO.id, C_ptkC4l_errMsgAccessDenied);
					} // else if (tablePermission == E_db_tablePermissionReadWrite)
				} // if ((ptkC4l_replaceBSrToBS0 (pCmdStr) == 0)
				break;
			case '\r':
				if (*pCmdStr == '\n')
				{
					tablePermission = db_tablePermission (pConnect, E_db_tableTypeAO, ethNumber);

					if ((tablePermission == E_db_tablePermissionRead) ||
						(tablePermission == E_db_tablePermissionReadWrite))
					{
						// AO lesen
						if (db_rowGet (pConnect, E_db_tableTypeAO, E_db_listTypeAO, dbAO.id, &dbAO) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@AO%d.%s\r\n",
									  dbAO.id, dbAO.strValue);
						} // if (db_rowGet (pConnect, E_db_tableTypeAO, &dbAO) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@AO%d%s\r\n",
									  dbAO.id, C_ptkC4l_errMsgDbRead);
						} // else if (db_rowGet (pConnect, E_db_tableTypeAO, E_db_listTypeAO, dbAO.id, &dbAO) == 0)
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@AO%d%s\r\n",
								  dbAO.id, C_ptkC4l_errMsgAccessDenied);
					} // else if ((tablePermission == E_db_tablePermissionReadWrite) || ...
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} /* switch (*pCmdStr++) */
	} // if (dbAO.id > 0)
} /* ptkC4l_evaluateAO */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateEV
 *
 *  \details	evaluiert den EV Command String. 
 * 
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu EV geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die Variablen können nur gelesen werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateEV (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_ev	dbEV;
	int			idinCmd;	// wird für die Antwort verwendet

	
	idinCmd = ptkC4l_getNumber (&pCmdStr);
	dbEV.id = idinCmd - 1;

	if (idinCmd > 0)
	{
		switch (*pCmdStr++)
		{
			case '\r':
				if (*pCmdStr == '\n')
				{
					if (db_tablePermission (pConnect, E_db_tableTypeEV, ethNumber) == E_db_tablePermissionRead)
					{
						// EV lesen
						if (db_rowGetSorted (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV) == 0)
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@EV%d.%s.%s\r\n",
									  idinCmd, dbEV.strValue, dbEV.strDateTime);
						} // if (db_rowGet (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@EV%d%s\r\n",
									  idinCmd, C_ptkC4l_errMsgDbRead);
						} // else
					}
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@EV%d%s\r\n",
								  dbEV.id, C_ptkC4l_errMsgAccessDenied);
					} // else if ((db_tablePermission (pConnect, E_db_tableTypeEV, ethNumber) == E_db_tablePermissionRead)
				} // if (*pCmdStr == '\n')
				break;
			default:
				break;
		} // switch (*pCmdStr++)
	} // if (dbEV.id > 0)
} /* ptkC4l_evaluateEV*/


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateFC
 *
 *  \details	evaluiert den FC Command String. 
 * 
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu FC geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 *	\attention	die Variablen können nur geschriebn werden.
 *
 ******************************************************************************/
static void ptkC4l_evaluateFC (ts_db_connect *pConnect,
							   char			 *pCmdStr,
							   char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_flag	dbFLAG;
	int			functionCode = ptkC4l_getNumber (&pCmdStr);


	switch (*pCmdStr++)
	{
		case '\r':
			if (*pCmdStr == '\n')
			{
				// ACHTUNG! E_db_tablePermissionRead bedeutet hier Schreibrecht, da FC nicht gelesen werden kann
				if (db_tablePermission (pConnect, E_db_tableTypeFC, ethNumber) == E_db_tablePermissionRead)
				{
					switch(functionCode)
					{
						case 0:
							ptkC4l_serviceModeActiv = 0;
							dbFLAG.id = E_db_flagServiceMode;
							dbFLAG.value = 0;
							if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC00\r\n");
							} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC00%s\r\n",
										  C_ptkC4l_errMsgDbWrite); 
							} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							break;

						case 1:
							ptkC4l_serviceModeActiv = 1;
							dbFLAG.id = E_db_flagServiceMode;
							dbFLAG.value = 1;
							if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC01\r\n");
							} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC01%s\r\n",
										  C_ptkC4l_errMsgDbWrite); 
							} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							break;

						case 30:
							dbFLAG.id = E_db_flagInitEventlist;
							dbFLAG.value = 1;

							if (ptkC4l_serviceModeActiv == 1)
							{
								if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								{
									fprintf (stderr, "ptkC4l_evaluateFC: flag 'initEventList' set\n");
									snprintf (pReplyStr, C_db_dataStringSize, "@FC30\r\n");
								} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC30%s\r\n",
											  C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							} // if (ptkC4l_serviceModeActiv == 1)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
										  functionCode, C_ptkC4l_errMsgValueProtected);
							} // else: if (ptkC4l_serviceModeActiv == 1)
							break;

						case 32:
							dbFLAG.id = E_db_flagInitParameter;
							dbFLAG.value = 1;
							if (ptkC4l_serviceModeActiv == 1)
							{
								if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC32\r\n");
								} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC32%s\r\n",
											  C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							} // if (ptkC4l_serviceModeActiv == 1)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
										  functionCode, C_ptkC4l_errMsgValueProtected);
							} // else: if (ptkC4l_serviceModeActiv == 1)
							break;

						case 33:
							dbFLAG.id = E_db_flagReconfigEthernet;
							dbFLAG.value = 1;
							if (ptkC4l_serviceModeActiv == 1)
							{
								if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC33\r\n");
								} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC33%s\r\n",
											  C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							} // if (ptkC4l_serviceModeActiv == 1)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
										  functionCode, C_ptkC4l_errMsgValueProtected);
							} // else: if (ptkC4l_serviceModeActiv == 1)
							break;

						case 34:
							dbFLAG.id = E_db_flagReconfigNTP;
							dbFLAG.value = 1;
							if (ptkC4l_serviceModeActiv == 1)
							{
								if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC%d\r\n",functionCode);
								} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
											  functionCode, C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							} // if (ptkC4l_serviceModeActiv == 1)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
										  functionCode, C_ptkC4l_errMsgValueProtected);
							} // else: if (ptkC4l_serviceModeActiv == 1)
							break;

						case 99:
							dbFLAG.id = E_db_flagReboot;
							dbFLAG.value = 1;

							if (ptkC4l_serviceModeActiv == 1)
							{
								if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC%d\r\n",functionCode);
								} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
								else
								{
									snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
											  functionCode, C_ptkC4l_errMsgDbWrite); 
								} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
							} // if (ptkC4l_serviceModeActiv == 1)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
										  functionCode, C_ptkC4l_errMsgValueProtected);
							} // else: if (ptkC4l_serviceModeActiv == 1)
							break;

						default:
							break;
					} // switch(functionCode)
				}
				else
				{
					snprintf (pReplyStr, C_db_dataStringSize, "@FC%d%s\r\n",
							  functionCode, C_ptkC4l_errMsgAccessDenied);
				} // else if ((db_tablePermission (pConnect, E_db_tableTypeEV, ethNumber) == E_db_tablePermissionRead)
			} // if (*pCmdStr == '\n')
			break;

		default:
			break;
	} /* switch (*pCmdStr++) */
} /* ptkC4l_evaluateFC */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateSC
 *
 *  \details	evaluiert den SC Command String. 
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den bis zu SC geparsten Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\return		-
 *
 ******************************************************************************/
static void ptkC4l_evaluateSC (ts_db_connect *pConnect,
                               char			 *pCmdStr,
                               char			 *pReplyStr,
							   int			 ethNumber)
{
	ts_db_va			  dbVA;
	ts_db_pa		 	  dbPA;
	te_db_tablePermission tablePermission;
	char				  commandStr[C_ptkC4l_commandStrLength];
	int					  ntpPort;
	FILE 				  *fp;
	char 				  line[130];
	char 		 		  *pPosLF;
	char 				  *pPosDateStr;

	
	dbVA.id = (int)E_db_VAidSystemTimeFormat3;
	dbPA.id	= C_db_paBaseNTP;
	
	switch (*pCmdStr++)
	{
		case '.':
			// Date Time schreiben
			if (ptkC4l_replaceChar1withChar2 (pCmdStr, '\r', '\0') == 0)
			{
				tablePermission = db_tablePermission (pConnect, E_db_tableTypeSC, ethNumber);

				if (tablePermission == E_db_tablePermissionReadWrite)
				{
					if (ptkC4l_serviceModeActiv == 1)
					{
						if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
						{
							ntpPort = atoi(dbPA.strValue);
							if (ntpPort == 0)
							{
								snprintf (commandStr, C_ptkC4l_commandStrLength, "sudo timedatectl set-time '%s'", pCmdStr);
								//fprintf (stderr,"\n- %s\n", commandStr);
								system (commandStr);
								snprintf (pReplyStr, C_db_dataStringSize, "@SC.%s\r\n",pCmdStr);
							}  // if (ntpPort == 0)
							else
							{
								snprintf (pReplyStr, C_db_dataStringSize, "@SC%s\r\n",
										  C_ptkC4l_errMsgValueProtected);
							} // else: if (ntpPort == 0)
						} //if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
						else
						{
							snprintf (pReplyStr, C_db_dataStringSize, "@SC%s\r\n",
									  C_ptkC4l_errMsgDbRead);
						} // else: if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
					} // if (ptkC4l_serviceModeActiv == 1)
					else
					{
						snprintf (pReplyStr, C_db_dataStringSize, "@SC%s\r\n",
								  C_ptkC4l_errMsgValueProtected);
					} // else if (ptkC4l_serviceModeActiv == 1)
				}
				else
				{
					snprintf (pReplyStr, C_db_dataStringSize, "@SC%s\r\n",
							  C_ptkC4l_errMsgAccessDenied);
				} // else if (tablePermission == E_db_tablePermissionReadWrite)
			} // if ((ptkC4l_replaceBSrToBS0 (pCmdStr) == 0)
			break;
		case '\r':
			if (*pCmdStr == '\n')
			{
				tablePermission = db_tablePermission (pConnect, E_db_tableTypeSC, ethNumber);

				if ((tablePermission == E_db_tablePermissionRead) ||
					(tablePermission == E_db_tablePermissionReadWrite))
				{
					fp = popen("timedatectl --value", "r");		/* Issue the command.		*/

					while ( fgets( line, sizeof line, fp))
					{
						//printf("%s", line);
						break;
					}
					pclose(fp); 
					
					pPosLF = memchr(line, '\n', strlen(line));
					*pPosLF = '\0';
					
					pPosDateStr = memchr(line, ':', strlen(line));
					snprintf (pReplyStr, C_db_dataStringSize, "@SC.%s\r\n",
							  (pPosDateStr+2));
				}
				else
				{
					snprintf (pReplyStr, C_db_dataStringSize, "@SC%s\r\n",
							  C_ptkC4l_errMsgAccessDenied);
				} // else if ((tablePermission == E_db_tablePermissionRead) || ...
			} // if (*pCmdStr == '\n')
			break;
		default:
			break;
	} /* switch (*pCmdStr++) */
} /* ptkC4l_evaluateSC */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		ptkC4l_evaluateCmd
 *
 *  \details	evaluiert den Command String. Wird der ReplyStr leer zurück-
 *				gegeben, generiert die aufrufende Funktion eine Fehlermeldung
 *				aus dem CmdStr.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *  \param		*pCmdStr	Pointer auf den Command String
 *  \param		*pReplyStr	Pointer auf den Antwort String
 *	\param		ethNumber	Ethernet Schnitstellen Nummer (0..2)
 *
 *	\attention	ReplyStr muss gelöscht übergeben werden.
 *
 *	\return		-
 *
 ******************************************************************************/
void ptkC4l_evaluateCmd (ts_db_connect	*pConnect,
                         char			*pCmdStr,
                         char			*pReplyStr,
                         int			ethNumber)

{
	if (*pCmdStr++ == '@')
	{
		switch (*pCmdStr++)
		{
			case 'P':
				if (*pCmdStr++ == 'A')
				{
					ptkC4l_evaluatePA (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

			case 'T':
				if (*pCmdStr++ == 'P')
				{
					ptkC4l_evaluateTP (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

			case 'V':
				if (*pCmdStr++ == 'A')
				{
					ptkC4l_evaluateVA (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

			case 'D':
				switch (*pCmdStr++)
				{
					case 'I':
						ptkC4l_evaluateDI (pConnect, pCmdStr, pReplyStr, ethNumber);
						break;
						
					case 'O':
						ptkC4l_evaluateDO (pConnect, pCmdStr, pReplyStr, ethNumber);
						break;
						
					case 'L':
						ptkC4l_evaluateDL (pConnect, pCmdStr, pReplyStr, ethNumber);
						break;
						
				} // switch (pCmdStr [pos])
				break;

			case 'A':
				switch (*pCmdStr++)
				{
					case 'I':
						ptkC4l_evaluateAI (pConnect, pCmdStr, pReplyStr, ethNumber);
						break;
						
					case 'O':
						ptkC4l_evaluateAO (pConnect, pCmdStr, pReplyStr, ethNumber);
						break;
						
				} // switch (pCmdStr [pos])
				break;

			case 'E':
				if (*pCmdStr++ == 'V')
				{
					ptkC4l_evaluateEV (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

			case 'F':
				if (*pCmdStr++ == 'C')
				{
					ptkC4l_evaluateFC (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

			case 'S':
				if (*pCmdStr++ == 'C')
				{
					ptkC4l_evaluateSC (pConnect, pCmdStr, pReplyStr, ethNumber);
				}
				break;

		} // switch (*pCmdStr++)
	} // if (*pCmdStr++ == '@')
} /* ptkC4l_evaluateCmd */


/**************************************************************************//**
 *
 *  \brief		ptkC4l_replaceBSrToBS0
 *
 *  \details	sucht im Command String nach dem ersten \r und ersetzt dieses
 *				durch \0.
 *
 *	\param		*pCmdStr	Pointer auf den Commando String
 *	\param		char1		Zeichen welches ersetzt werden soll
 *	\param		char2		Ersatzzeichen
 *
 *	\return		0: Ersetzung durchgeführt
 * 				1: Ersetzung konnte nicht durchgeführt werden
 *
 ******************************************************************************/
int ptkC4l_replaceChar1withChar2 (char *pCmdStr,
								  char char1,
								  char char2)
{
	int	 retValue = 0;
	char *pPos = memchr (pCmdStr, char1, (C_db_dataStringSize - C_ptkC4l_PAmaxHeaderLenWrite));

	
	if (pPos == NULL)
	{
		retValue = 1;
	}
	else
	{
		*pPos = char2;
	}

	return (retValue);
} /* ptkC4l_replaceBSrToBS0 */

/* End ptkC4l.c */
