/**************************************************************************//**
 *
 *	\file		buildDate.h
 *
 *	\brief		Headerfile der Funktionen für das build date des compilers.
 *
 *	\details	-
 *
 *	\date		17.11.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	17.11.21 mm
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __buildDate_H
    #define __buildDate_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_buildDate_year_CH0 (__DATE__[ 9])
#define C_buildDate_year_CH1 (__DATE__[ 10])

#define C_buildDate_month_is_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define C_buildDate_month_is_FEB (__DATE__[0] == 'F')
#define C_buildDate_month_is_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define C_buildDate_month_is_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define C_buildDate_month_is_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define C_buildDate_month_is_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define C_buildDate_month_is_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define C_buildDate_month_is_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define C_buildDate_month_is_SEP (__DATE__[0] == 'S')
#define C_buildDate_month_is_OCT (__DATE__[0] == 'O')
#define C_buildDate_month_is_NOV (__DATE__[0] == 'N')
#define C_buildDate_month_is_DEC (__DATE__[0] == 'D')

#define C_buildDate_month_CH0 \
    ((C_buildDate_month_is_OCT || C_buildDate_month_is_NOV || C_buildDate_month_is_DEC) ? '1' : '0')

#define C_buildDate_month_CH1 \
    ( \
        (C_buildDate_month_is_JAN) ? '1' : \
        (C_buildDate_month_is_FEB) ? '2' : \
        (C_buildDate_month_is_MAR) ? '3' : \
        (C_buildDate_month_is_APR) ? '4' : \
        (C_buildDate_month_is_MAY) ? '5' : \
        (C_buildDate_month_is_JUN) ? '6' : \
        (C_buildDate_month_is_JUL) ? '7' : \
        (C_buildDate_month_is_AUG) ? '8' : \
        (C_buildDate_month_is_SEP) ? '9' : \
        (C_buildDate_month_is_OCT) ? '0' : \
        (C_buildDate_month_is_NOV) ? '1' : \
        (C_buildDate_month_is_DEC) ? '2' : \
        /* error default */    '?' \
    )

#define C_buildDate_day_CH0 ((__DATE__[4] >= '0') ? (__DATE__[4]) : '0')
#define C_buildDate_day_CH1  (__DATE__[5])

#define C_buildDate_hour_CH0  (__TIME__[0])
#define C_buildDate_hour_CH1  (__TIME__[1])

#define C_buildDate_min_CH0  (__TIME__[3])
#define C_buildDate_min_CH1  (__TIME__[4])


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/


#endif //__buildDate_H

/* End buildDate.h */
