/**************************************************************************//**
 *
 *	\file		mail.c
 *
 *	\brief		Stellt die Funktionen für die Versendung von Mails zur Verfügung.
 *
 *	\details	-
 *
 *	\date		11.05.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	11.05.21 mm
 *					- Erstellt.
 *	\version	03.09.21 mm
 *					- Anpassungen für mailx.
 *	\version	30.09.21 mm
 *					- in mail_getDbText Parameter dbIdOffset auf dbId geändert,
 *					  da beim Lesen des Accounts ein völlig anderer Offset benötigt
 *					  wird.
 *					- Konfigurationsmodifikationen von Techdock issue #17 vom
 *					  24.09.21 implementiert.
 *	\version	22.10.21 mm
 *					- Funktion mail_configDebug implementiert.
 *	\version	09.12.21 mm
 *					- Funktion mail_configWriteToMsmtprc mit statusmeldungen
 *					  ausgestattet.
 *					- um das Problem mit den Zugriffsrechten zu lösen, wird das
 *					  File msmtprc in /tmp/msmtprc geschriben und anschliessend
 *					  mit dem system Befehl nach /etc/msmtprc verschoben.
 *	\version	02.05.22 mm
 *					- Funktion mail_configWriteToMsmtprc um neuen Tag passwordeval
 *					  gemäss Issue #90 erweitert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <errno.h>
#include <stdlib.h>
// #include <mysql/mysql.h>
#include <stdio.h>
#include <string.h>

#include "csw.h"
#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_mail_pathLen		255


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		mail_getDbValue
 *
 *	\details	liest den Konfigurationswert dbIdOffset für die Mailfunktion
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		Integerwert aus Datenbank
 *
 ******************************************************************************/
static int mail_getDbValue (ts_db_connect		 *pConnect,
							te_db_PAidOffsetSMTP dbIdOffset,
							int					 *pFail)
{
	static ts_db_pa dbPA;


	dbPA.id = C_db_paBaseSMTP + dbIdOffset;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		return (atoi (dbPA.strValue));
	}
	else
	{
		*pFail = 1;

		return (0);
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* mail_getDbValue */


/**************************************************************************//**
 *
 *	\brief		mail_getDbText
 *
 *	\details	liest den Konfigurationstext dbIdOffset für die Mailfunktion
 *				aus der Datenbanktabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		dbIdOffset	Datenbank id Offset zur Basis id des Eintrags.
 *	\param		*pText		Pointer auf den gelesenen Text.
 *	\param		*pFail		Resultat Flag, wird bei einem Fehler auf 1 gesetzt
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
static void mail_getDbText (ts_db_connect	*pConnect,
							int				dbId,
							char			*pText,
							int				*pFail)
{
	ts_db_pa	dbPA;


	dbPA.id = dbId;

	if (db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
	{
		snprintf (pText, C_db_dataStringSize, "%s", dbPA.strValue);
	}
	else
	{
		*pFail = 1;
	} // else if (db_rowGet (pConnect, E_db_tableTypePA, &dbPA) == 0)
} /* mail_getDbText */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		mail_configWriteToMsmtprc
 *
 *	\details	schreibt das File msmtprc mit der aktuellen Konfiguration
 *				komplett neu.
 *
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int mail_configWriteToMsmtprc (ts_mail	*pMail)
{
	FILE	*pFile;
	int		retValue = 0;


	fprintf (stderr, "mail_configWriteToMsmtprc: ");

	pFile = fopen ("/tmp/msmtprc", "w");

	if (pFile == NULL)
	{
		retValue = errno;
		fprintf (stderr, "open /tmp/msmtprc failed, error %d\n", retValue);
	}
	else
	{
		fprintf (pFile, "#this file was generated automatically by ioLog\n\n");
		fprintf (pFile, "#set default values for all accounts\n");
		fprintf (pFile, "defaults\n");

		switch (pMail->encryption)
		{
			case E_mail_encryptOff:
				fprintf (pFile, "auth off\n");
				fprintf (pFile, "tls off\n");
				fprintf (pFile, "tls_starttls off\n");
				break;

			case E_mail_encryptTLS:
				fprintf (pFile, "auth on\n");
				fprintf (pFile, "tls on\n");
				fprintf (pFile, "tls_starttls off\n");
				break;

			case E_mail_encryptSSL:
				fprintf (pFile, "auth on\n");
				fprintf (pFile, "tls on\n");
				fprintf (pFile, "tls_starttls on\n");
				break;

			default:
				break;

		} // switch (pMail->encryption)

		fprintf (pFile, "tls_trust_file /etc/ssl/certs/ca-certificates.crt\n");
		fprintf (pFile, "#logfile /var/log/msmtp.log\n\n");
		fprintf (pFile, "#email settings\n");
		fprintf (pFile, "account %s\n", pMail->account);
		fprintf (pFile, "host %s\n", pMail->server);
		fprintf (pFile, "port %d\n", pMail->port);
		fprintf (pFile, "from %s\n", pMail->from);
		fprintf (pFile, "user %s\n", pMail->user);

		// Passwort direkt gemäss PA138 oder in /home/c4l/c4.gpg
		if (strncmp(pMail->password, C_mail_passwordEvalTxt, C_mail_passwordEvalLen) == 0)
		{
			fprintf (pFile, "passwordeval %s\n\n", pMail->password);
		}
		else
		{
			fprintf (pFile, "password %s\n\n", pMail->password);
		}
		
		fprintf (pFile, "#set a default account\n");
		fprintf (pFile, "account default: %s\n", pMail->account);
		fclose (pFile);

		if (system ("sudo mv /tmp/msmtprc /etc/msmtprc") < 0)
		{
			fprintf (stderr, "moving msmtprc to /etc/msmtprc failed\n");
		}
		else
		{
			fprintf (stderr, "/etc/msmtprc successfully written\n");
		} // else if (system ("sudo mv /tmp/msmtprc /etc/msmtprc") < 0)
	} // else if (pFile == NULL)
} /* mail_configWriteToMsmtprc */


/**************************************************************************//**
 *
 *  \brief		mail_configDebug
 *
 *	\details	schreibt die Werte der übergebenen ts_mail Struktur auf stderr
 *				aus.
 *
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 ******************************************************************************/
void mail_configDebug (ts_mail *pMail)
{
	fprintf (stderr, "\033[1mmail_configDebug\033[0m\n");
	fprintf (stderr, "port        : %d\n", pMail->port);
	fprintf (stderr, "mode        : %d\n", pMail->mode);
	fprintf (stderr, "iteration   : %d\n", pMail->iteration);
	fprintf (stderr, "encryption  : %d\n", pMail->encryption);
	fprintf (stderr, "subjectId   : %d\n", pMail->subjectId);
	fprintf (stderr, "sendErrMsgId: %d\n", pMail->sendErrMsgId);
	fprintf (stderr, "account     : %s\n", pMail->account);
	fprintf (stderr, "from        : %s\n", pMail->from);
	fprintf (stderr, "to          : %s\n", pMail->to);
	fprintf (stderr, "server      : %s\n", pMail->server);
	fprintf (stderr, "username    : %s\n", pMail->user);
	fprintf (stderr, "password    : %s\n", pMail->password);
} /* mail_configDebug */


/**************************************************************************//**
 *
 *  \brief		mail_configRead
 *
 *  \details	liest die Konfiguration für die Mails aus der Datenbank-
 *				tabelle PA.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		0: Lesen erfolgreich
 *
 ******************************************************************************/
int mail_configRead (ts_db_connect	*pConnect,
					 ts_mail		*pMail)
{
	ts_db_pa	dbPA;
	int			fail = 0;

	pMail->port			= mail_getDbValue (pConnect, E_db_PAoSMTPPort, &fail);
	pMail->mode			= mail_getDbValue (pConnect, E_db_PAoSMTPMode, &fail);
	pMail->iteration	= mail_getDbValue (pConnect, E_db_PAoSMTPIteration, &fail);
	pMail->encryption	= mail_getDbValue (pConnect, E_db_PAoSMTPEncryption, &fail);
	pMail->subjectId	= mail_getDbValue (pConnect, E_db_PAoSMTPSubject, &fail);
	pMail->sendErrMsgId	= mail_getDbValue (pConnect, E_db_PAoSMTPErrEvent, &fail);

	mail_getDbText (pConnect, C_db_paBaseSystem + E_db_PAoAlgProjectNumber, pMail->account, &fail);

	mail_getDbText (pConnect, C_db_paBaseSMTP + E_db_PAoSMTPFrom, pMail->from, &fail);
	mail_getDbText (pConnect, C_db_paBaseSMTP + E_db_PAoSMTPTo, pMail->to, &fail);
	mail_getDbText (pConnect, C_db_paBaseSMTP + E_db_PAoSMTPServer, pMail->server, &fail);
	mail_getDbText (pConnect, C_db_paBaseSMTP + E_db_PAoSMTPUsername, pMail->user, &fail);
	mail_getDbText (pConnect, C_db_paBaseSMTP + E_db_PAoSMTPPassword, pMail->password, &fail);

	dbPA.id = E_db_PAoAlgLanguage;
	db_rowGet (pConnect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA);

	pMail->languageOffset = 1000 * atoi (dbPA.strValue);
	
	return (fail);
} /* mail_configRead */


/**************************************************************************//**
 *
 *  \brief		mail_configOverride
 *
 *  \details	überschreibt die Konfiguration mit Testwerten.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 *	\return		-
 *
 ******************************************************************************/
void mail_configOverride (ts_db_connect	*pConnect,
						  ts_mail		*pMail)
{ 
	pMail->port	= 587;
	pMail->mode	= E_mail_modeOnce;

	snprintf (pMail->from, C_db_dataStringSize, "trimada@control4log.ch");
	snprintf (pMail->to, C_db_dataStringSize, "marcel.ming@trimada.ch");
	snprintf (pMail->server, C_db_dataStringSize, "smtp.swizzonic-mail.ch");

	pMail->iteration = 1;

	snprintf (pMail->user, C_db_dataStringSize, "trimada@control4log.ch");
	snprintf (pMail->password, C_db_dataStringSize, "Master-5610");

	pMail->encryption	= E_mail_encryptTLS;
	pMail->subjectId	= C_db_tpIdSMTPsubject;
	pMail->sendErrMsgId	= C_db_tpIdSMTPeth0failed;

	pMail->languageOffset = 1000,

	snprintf (pMail->subject, C_db_dataStringSize, "");
	snprintf (pMail->message, C_db_dataStringSize, "");
} /* mail_configOverride */


/**************************************************************************//**
 *
 *  \brief		mail_initSubject
 *
 *  \details	initialisiert den Eintrag subject in der Struktur ts_mail aus
 *				der Datenbanktabelle TP.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *
 ******************************************************************************/
void mail_initSubject (ts_db_connect *pConnect,
					   ts_mail		 *pMail)
{
	ts_db_tp	dbTP;


	dbTP.id	= pMail->languageOffset + C_db_tpIdSMTPsubject;
	db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

	snprintf (pMail->subject, C_db_dataStringSize, "%s", dbTP.strText);
} /* mail_initSubject */


#ifdef __csw_mail_useSendmail
/**************************************************************************//**
 *
 *	\brief		mail_send
 *
 *	\details	sendet ein Mail an den Empfänger. Benötigt sendmail.
 *
 *	\param		*to			Pointer auf die Empfängermailadresse.
 *	\param		*from		Pointer auf die Sendermailadresse.
 *	\param		*subject	Pointer auf den Mailtitel.
 *	\param		*message	Pointer auf die Mailtext.
 *
 *	\return		retValue
 *
 ******************************************************************************/
int mail_send  (const char *from,
				const char *to,
//				const char *cc,
//				const char *bcc,
				const char *subject,
				const char *message)
{
	FILE	*pPipe;
	FILE	*pPath;
	char	sendmailPath [C_mail_pathLen];
	int		retValue = 0;


	// Pfad zu sendmail ermitteln
	pPath = popen ("which sendmail", "r");

	if (pPath == NULL)
	{
		fprintf (stderr, "mail_send: error sendmail not found\n");
		retValue = EXIT_FAILURE;
	}
	else
	{
		fscanf (pPath, "%254s", sendmailPath);
		pclose (pPath);
		strcat (sendmailPath, " -n -oi -t");

		pPipe = popen (sendmailPath, "w");		// meistens /usr/sbin/sendmail

		if (pPipe == NULL)
		{
			fprintf (stderr, "no cnnection to sendmail\n");
			retValue = EXIT_FAILURE;
		}
		else
		{
			fprintf (pPipe, "From: %s\n", from);
			fprintf (pPipe, "To: %s\n", to);
//			fprintf (pPipe, "Cc: %s\n", cc);
//			fprintf (pPipe, "Bcc: %s\n", bcc);
			fprintf (pPipe, "Subject: %s", subject);

			// extra-Newline vorher ist hier wichtig ...-v
/*
			if (message != NULL)
			{
				fprintf (pPipe, "\n%s\n.\n", message);
			}
*/
			fprintf (pPipe, "\n%s\n.\n", message);

			pclose (pPipe);

			fprintf (stderr, "\033[1;36mmail_send: from: %s, to: %s, subject: %s, message:%s\033[0m\n", from, to, subject, message);
		} // else if (pPipe == NULL)
	} // else if (pPath == NULL)

	return (retValue);
} /* mail_send */
#endif // __csw_mail_useSendmail


#ifdef __csw_mail_useMailx
/**************************************************************************//**
 *
 *	\brief		mail_send
 *
 *	\details	sendet ein Mail an den Empfänger. Benötigt mailx.
 *
 *	\param		*to			Pointer auf die Empfängermailadresse.
 *	\param		*from		Pointer auf die Sendermailadresse.
 *	\param		*subject	Pointer auf den Mailtitel.
 *	\param		*message	Pointer auf die Mailtext.
 *
 *	\return		retValue
 *
 ******************************************************************************/
int mail_send  (const char *from,
				const char *to,
				const char *subject,
				const char *message)
{
	int		retValue = 0;
	char	commandStr [C_mail_dataStringSize];


	snprintf (commandStr, C_mail_dataStringSize, "echo \"%s\" | mailx -s \"%s\" %s &",
				message, subject, to);

	retValue = system (commandStr);

	#ifdef __mail_showDebugStrings
		fprintf (stderr, "mail_send: %s (return %d)\n", commandStr, retValue);
	#endif

	if (retValue != 0)
	{
		// Fehlermeldung
		snprintf (commandStr, C_mail_dataStringSize, "echo \"message too long\" | mailx -s \"error\" %s &", to);

		retValue = system (commandStr);

		#ifdef __mail_showDebugStrings
			fprintf (stderr, "mail_send: %s (return %d)\n", commandStr, retValue);
		#endif
	} // if (retValue != 0)

	return (retValue);
} /* mail_send */
#endif // __csw_mail_useMailx


/* mail.c */

