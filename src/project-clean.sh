#! /bin/bash

# file          project-clean.sh
# brief         script for project vpn4log
# details       deletes all builded file output from make of project
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       07.04.2023      mimi    - creating
#

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)

printf "\n### Running ${filename} to install project ${project}-files and symlinks\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# start cleaning

## delete all build libs of make configeths
path=$srcdir/tools/configeths
#ls -la
sudo rm $path/configeths.o
sudo rm $path/csv.o
sudo rm $path/db.o
#ls -la
printf "All build libs from make configeths by ${filename} are delete\n"

## delete all build libs of make configntp
path=$srcdir/tools/configntp
#ls -la
sudo rm $path/configntp.o
sudo rm $path/csv.o
sudo rm $path/db.o
#ls -la
printf "All build libs from make configntp by ${filename} are delete\n"

## delete all build libs of make conv2utf8
path=$srcdir/tools/conv2utf8
#ls -la
sudo rm $path/conv2utf8.o
#ls -la
printf "All build libs from make conv2utf8 by ${filename} are delete\n"

## delete all build libs of make dbcreate
path=$srcdir/tools/dbcreate
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/dbcreate.o
#ls -la
printf "All build libs from make dbcreate by ${filename} are delete\n"

## delete all build libs of make dbdelete
path=$srcdir/tools/dbdelete
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/dbdelete.o
#ls -la
printf "All build libs from make dbdelete by ${filename} are delete\n"

## delete all build libs of make usbevent
path=$srcdir/tools/usbevent
#ls -la
sudo rm $path/c4lusb.o
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/mail.o
sudo rm $path/ptkC4l.o
sudo rm $path/usbevent.o
#ls -la
printf "All build libs from make usbevent by ${filename} are delete\n"

## delete all build libs of make usbhello
path=$srcdir/tools/usbhello
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/usbhello.o
#ls -la
printf "All build libs from make usbhello by ${filename} are delete\n"

## delete all build libs of make tcpss
path=$srcdir/tcpss
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/ptkC4l.o
sudo rm $path/tcpss.o
#ls -la
printf "All build libs from make tcpss by ${filename} are delete\n"

## delete all build libs of make shutdown
path=$srcdir/tools/shutdown
#ls -la
sudo rm $path/csv.o
sudo rm $path/db.o
sudo rm $path/shutdown.o
#ls -la
printf "All build libs from make shutdown by ${filename} are delete\n"

## delete all symbolic links for make configets
path=$srcdir/tools/configeths
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make configeths are by ${filename} delete\n"

## delete all symbolic links for make configntp
path=$srcdir/tools/configntp
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make configntp are by ${filename} delete\n"

## delete all symbolic links for make conv2utf8
path=$srcdir/tools/conv2utf8
sudo rm $path/buildDate.h
printf "All symlinks for make conv2utf8 are by ${filename} delete\n"

## delete all symbolic links for make dbcreate
path=$srcdir/tools/dbcreate
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make decreate are by ${filename} delete\n"

## delete all symbolic links for make dbdelete
path=$srcdir/tools/dbdelete
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make dbdelete are by ${filename} delete\n"

## delete all symbolic links for make usbevent
path=$srcdir/tools/usbevent
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/c4lusb.c
sudo rm $path/c4lusb.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/mail.c
sudo rm $path/mail.h
sudo rm $path/ptkC4l.c
sudo rm $path/ptkC4l.h
#ls -la
printf "All symlinks for make usbevent are by ${filename} delete\n"

## delete all symbolic links for make usbhello
path=$srcdir/tools/usbhello
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
#ls -la
printf "All symlinks for make usbhello are by ${filename} delete\n"

## delete all symbolic links for make tcpss
path=$srcdir/tcpss
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/ptkC4l.c
sudo rm $path/ptkC4l.h
#ls -la
printf "All symlinks for make tcpss are by ${filename} delete\n"

## delete all symbolic links for make shutdown
path=$srcdir/tools/shutdown
#ls -la
sudo rm $path/buildDate.h
sudo rm $path/csv.c
sudo rm $path/csv.h
sudo rm $path/csw.h
sudo rm $path/db.c
sudo rm $path/db.h
sudo rm $path/mail.c
sudo rm $path/mail.h

#ls -la
printf "All symlinks for make shutdown are by ${filename} delete\n"


# end skript
printf "### end ${filename}\n\n"
