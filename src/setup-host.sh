#! /bin/bash

# file          project-start.sh
# brief         script for project vpn4log
# details       starting all running services from project
# date          14.05.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       14.05.2023      mimi    - creating
#

# declare script variables and dierctories
project="vpn4log"
name="v4l"
filename=$(basename $0)
user=$(whoami)
todfile=$(date +'%d.%m.%Y_%H:%M:%S')
tod=$(date)
ver=$(mysql --database=testBase --user=trimada --password=Master-5610 -s -N --execute="SELECT value FROM PA WHERE id = 0;")

printf "\n### Running ${filename} to start project ${project}, v${ver} services as user ${user} at ${tod}\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Source directory is:    ${srcdir}\n"

# navigate to prodir /home/<user>/projects/<project>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent

# create prodir = /home/<user>/projects/<project>/
prodir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Project directory is:    ${prodir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}" # will get back current directory where this skript is running
printf "Binary directory is:    ${bindir}\n"

# navigate to homedir /home/<user>/
cd prodir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../../ > /dev/null 2>&1 # redirect output for make cmd silent

# create homedir = /home/<user>/
homedir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)" # will get back current directory where this skript is running
printf "Home directory is:    ${srcdir}\n"

# create docdir = /home/<user>/Documents/
docdir="${homedir}/Documents"
printf "Documents directory is:    ${srcdir}\n"

# create backdir = /home/<user>/Documents/<ver>-backup/
backdir="${docdir}/${ver}-backup"
printf "Backup directory is:    ${backdir}\n"

# starting process
printf "\n### starting setup for ${project}\n\n"


## set serial no. pa9
printf "\n### PA9 current:\n"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SELECT * FROM PA WHERE id = 9;"

printf "\n### input serial number: "
read pa9
mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE PA SET value = '${pa9}' WHERE id = 9;"

printf "\n### PA9 new:\n"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SELECT * FROM PA WHERE id = 9;"
printf "\n\n"


## set serial no. pa9 in STD PA-CSV file 11007-PA-default.csv

### create directory
sudo mkdir $backdir

### make backup from current CSV file 11007-PA-default.csv
sudo cp $srcdir/db/11007-PA-default.csv $backdir/11007-PA-default.csv.$todfile

### replace full line in CSV which contains V4L0-YYMM-nnnn-nnnn
sudo sed -i "/9;;V4L0-YYMM-nnnn-nnnn;;;0/c\9;;${pa9};;;0" $srcdir/db/11007-PA-default.csv


## set hostname in /etc/hostname
printf "\n### /etc/hostname current:\n"
hostname

### make backup from current file /etc/hostname
sudo mv /etc/hostname $backdir/hostname.$todfile

### create new file /etc/hostname
cat > /etc/hostname <<EOF
$pa9
EOF

printf "\n### /etc/hostname new:\n"
cat /etc/hostname


## set hostname in /etc/hosts
printf "\n### /etc/hosts current:\n"
cat /etc/hosts

### make backup from current file /etc/hosts
sudo cp /etc/hosts $backdir/hosts.$todfile

### replace full line which contains 127.0.1.1
sudo sed -i "/127.0.1.1/c\127.0.1.1\t${pa9}" /etc/hosts

printf "\n### /etc/hosts new:\n"
cat /etc/hosts


## restart  device

printf "\n### FLAG current:\n"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SELECT * FROM FLAG;"

printf "\n### set FC32: set standard PA's"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE FLAG SET value = 1 WHERE id = 2;"

printf "\n### set FC33: set new ip's"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE FLAG SET value = 1 WHERE id = 3;"

printf "\n### set FC34: set new ntp host"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE FLAG SET value = 1 WHERE id = 4;"

printf "\n### FLAG new:\n"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="SELECT * FROM FLAG;"
printf "\n\n"

### ... and now reboot device!
printf "\n### set FC99: rebooting and execute PA changes"
mysql --database=testBase --user=trimada --password=Master-5610 --execute="UPDATE FLAG SET value = 1 WHERE id = 5;"


# end skript
printf "### end ${filename}\n\n"
