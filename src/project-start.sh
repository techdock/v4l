#! /bin/bash

# file          project-start.sh
# brief         script for project vpn4log
# details       starting all running services from project
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license	The MIT License (MIT)
#
# version       07.04.2023	mimi	- creating
# 

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)

printf "\n### Running ${filename} to start project ${project} services\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent


# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# starting process

## change rights for all v4l services
sudo chmod 744 $srcdir/tools/usbevent/usbevent.service
sudo chmod 744 $srcdir/tools/configeths/configeths.service
sudo chmod 744 $srcdir/tools/configntp/configntp.service
sudo chmod 744 $srcdir/tcpss/tcpss.service
sudo chmod 744 $srcdir/tools/shutdown/shutdown.service
printf "Changed rights for all ${filename} running services\n"

## add all v4l services to systemctl list
sudo systemctl enable configeths.service
sudo systemctl enable configntp.service
sudo systemctl enable tcpss.service
sudo systemctl enable usbevent.service
sudo systemctl enable shutdown.service
printf "Add all ${filename} running services to systemctl list\n"

## start all v4l services
printf "INFO: Starting services with ${filename} will take approx. 60s.\n"
sudo systemctl start configeths.service
sudo systemctl start configntp.service
sudo systemctl start tcpss.service
sudo systemctl start usbevent.service
sudo systemctl start shutdown.service
printf "Started all ${filename} services\n"

## load  systemctl list new for make sure services are started when rebooting
sudo systemctl daemon-reload
printf "Reloaded systemctl\n"

# end skript
printf "### end ${filename}\n\n"

