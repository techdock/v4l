#! /bin/bash

# file          project-install.sh
# brief         script for project vpn4log
# details       delete all project files to /opt/v4l
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       07.04.2023      mimi    - creating
#

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)

printf "\n### Running ${filename} to install project ${project}-files and symlinks\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"

# create srcdir = /home/<user>/projects/<project>/src/
srcdir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)"
printf "Source directory is:    ${srcdir}\n"

# create bindir = /home/<user>/projects/<project>/opt/<name>/
cd srcdir > /dev/null 2>&1 # redirect output for make cmd silent
cd ../ > /dev/null 2>&1 # redirect output for make cmd silent


# bindir = /home/<user>/projects/<project>/opt/<name>/
bindir="$(cd -P -- "$(dirname -- "$0")" && pwd -P)${instdir}"
printf "Binary directory is:    ${bindir}\n"


# stop all project services in case they are still running

## stoppen aller c4l Dienste
sudo systemctl stop configeths
sudo systemctl stop configntp
sudo systemctl stop tcpss
sudo systemctl stop usbevent
sudo systemctl stop shutdown

## killen aller c4l prozess
sudo pkill configeths
sudo pkill configntp
sudo pkill tcpss
sudo pkill usbevent
sudo pkill shutdown

## entfernen aller c4l dienst aus der systemctl liste für autostart
sudo systemctl disable configeths
sudo systemctl disable configntp
sudo systemctl disable tcpss
sudo systemctl disable usbevent
sudo systemctl disable shutdown

## neu laden der systemctl liste für autostart
sudo systemctl daemon-reload


# delete db and binaries in instdir

## delete first db
sudo $instdir/dbdelete


# start uninstall binaries

## delete binaries in bindir
sudo rm $bindir/11007-PA-default.csv
sudo rm $bindir/11007-TP-default.csv
sudo rm $bindir/configeths
sudo rm $bindir/configntp
sudo rm $bindir/conv2utf8
sudo rm $bindir/dbcreate
sudo rm $bindir/dbdelete
sudo rm $bindir/dhcpcd.conf
sudo rm $bindir/interfaces-template
sudo rm $bindir/rt_tables
sudo rm $bindir/shutdown
sudo rm $bindir/tcpss
sudo rm $bindir/timesyncd-template.conf
sudo rm $bindir/usbevent
sudo rm $bindir/usbhello
cd $bibdir
#ls -la

## delete binaries in instdir
sudo rm $instdir/11007-PA-default.csv
sudo rm $instdir/11007-TP-default.csv
sudo rm $instdir/configeths
sudo rm $instdir/configntp
sudo rm $instdir/dbcreate
sudo rm $instdir/dbdelete
sudo rm $instdir/interfaces-template
sudo rm $instdir/shutdown
sudo rm $instdir/tcpss
sudo rm $instdir/tcpss-start.sh
sudo rm $instdir/timesyncd-template.conf
sudo rm $instdir/usbevent
sudo rm $instdir/usbhello
cd $instdir
#ls -la


# delete servie files

## delete configeths.service file
filedir=$srcdir/tools/configeths
sudo rm $filedir/configeths.service
printf "Delete configeths.service for ${project} in ${filedir}\n"

## delete configntp.service file
filedir=$srcdir/tools/configntp
sudo rm $filedir/configntp.service
printf "Delete configntp.service for ${project} in ${filedir}\n"

## delete shutdown.service file
filedir=$srcdir/tools/shutdown
sudo rm $filedir/shutdown.service
printf "Delete shutdown.service for ${project} in ${filedir}\n"

## delete usbevent.service file
filedir=$srcdir/tools/usbevent
sudo rm $filedir/usbevent.service
printf "Delete usbevent.service for ${project} in ${filedir}\n"

## delete tcpss.service file
filedir=$srcdir/tcpss
sudo rm $filedir/tcpss.service
printf "Delete tcpss.service for ${project} in ${filedir}\n"


# end skript
printf "### end ${filename}\n\n"
