#! /bin/bash

# file          usbplugged.sh
# brief         script for project vpn4log
# details       starting at a usb plug event the follow script as root
# date          07.04.2023
# author        Michael Mislin
# copyright     techdock GmbH, 4102 Binningen, Switzerland
# license       The MIT License (MIT)
#
# version       07.04.2023      mimi    - creating
#

# declare script variables and dierctories
project="v4l"
name="v4l"
filename=$(basename $0)

printf "\n### Running ${filename} to start project ${project} services\n"

# create instdir = /opt/<name>/
instdir="/opt/${name}"
printf "Install directory is:   ${instdir}\n"


# starting process
printf "starting usbhello at usb 1.1.1 ... .4 for ${name} of project ${project}"

su c4l $instdir/usbhello


# end skript
printf "### end ${filename}\n\n"
