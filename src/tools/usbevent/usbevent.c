 /**************************************************************************//**
 *
 *	\file		usbevent.c
 *
 *	\brief		führt die Aufgaben beim Einstecken eines USB Memorysticks aus.
 *
 *	\details	Zusatzprogramm für den I/O-Logger des techdock Projekts control4log.
 *				Folgende Aufgaben werden ausgeführt:
 *				- öffnen der Datenbank.
 *				- lesen der USB Konfiguration aus der Datenbank.
 *				- ausführen eventueller Uploads vom USB Massenspeicher in die
 *				  Datenbank.
 *				- ausführen eventueller Downloads von der Datenbank zum USB
 *				  Massenspeicher.
 *				- Events eventueller Up- oder Downloads in die EV Tabelle der
 *				  Datenbank schreiben.
 *
 *	\attention	Das Programm überwacht die USB schnittstelle nicht. Es muss vom
 *				System beim Erkennen eines Plug In Ereignisses aufgerufen werden.
 *				Dazu kann in /etc/udev/rules.d ein File custom_usb.rules mit
 *				folgendem Inhalt erstellt werden:
 *				- ACTION=="add", SBSYSTEM=="usb", ATTRS{bInterfaceClass}=="08", PROGRAM="/opt/11007/usbevent"
 *
 *	\date		08.06.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	08.06.21 mm
 *					- Erstellt.
 *	\version	13.08.21 mm
 *					- Ein USB Massenspeicher wird in /media/fat32 mit Schreib- und
 *					  Leserechten für alle gemountet, da der über custom_usb.rules
 *					  gestartete Prozess sonst nichts schreiben kann. 
 *	\version	10.11.21 mm
 *					- Mail Funktionen implementiert.
 *	\version	17.11.21 mm
 *					- C_usbevent_appVersion über builDate.h implementiert.
 *	\version	17.12.21 mm
 *					- neue Reigenfolge der Aufgabenabarbeitung:
 *						- abklärung Downloads
 *						- abklärung Uploads
 *					- nach einem Upload werden die Steuerfiles auf dem USB-Stick
 *					  gelöscht, für ioLog eine Parameterübernahme initiiert und
 *					  ein Neustart der Systems veranlasst.
 *					- Parameter in den Header nachgeführt / korrigiert.
 *
 *	\attention		Die Steuerfiles müssen vor einem Neustart gelöscht werden um
 *					nach dem Neustart eine Endlosschleife zu vermeiden.
 *
 *	\version	20.12.21 mm
 *					- bei einem benötigten Reboot müssen auch die Flags
 *						- Reconfig Ethernet
 *						- Reconfig NTP
 *					  gesetzt werden.
 *
 *	\attention		Das Flag Init Parameter darf hingegen nicht gesetz werde, da
 *					so die Default Parameter geladen werden und die Konfiguration
 *					über den USB Stick überschrieben wird.
 *
 *	\version	22.12.21 mm
 *					- bei Uploads wird die Zeichencodierung zuerst von wdTw zu
 *					  utf8 konvertiert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

// für usbevent_listDirectory
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>


#include "buildDate.h"
#include "c4lusb.h"
#include "db.h"
#include "mail.h"
#include "ptkC4l.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_usbevent_appName			"usbevent"

const uint8_t C_usbevent_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/


/**************************************************************************//**
 *
 *	\brief		usbevent_listRights
 *
 *	\details	listet die Berechtigungen in stat auf.
 *
 *	\param		*pAttribut		Pointer auf Struktur vom Typ stat
 *
 *	\return		-
 *
 ******************************************************************************/
void usbevent_listRights (struct stat *pAttribut)
{
	// Typ
	if (pAttribut->st_mode & S_IFDIR)
	{
		fprintf (stderr, "d");
	}
	else
	{
		fprintf (stderr, "-");
	}

	// Eigentümer
	if (pAttribut->st_mode & S_IRUSR)
	{
		fprintf (stderr, "r");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IWUSR)
	{
		fprintf (stderr, "w");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IXUSR)
	{
		fprintf (stderr, "x");
	}
	else
	{
		fprintf (stderr, "-");
	}

	// Gruppe des Eigentümers
	if (pAttribut->st_mode & S_IRGRP)
	{
		fprintf (stderr, "r");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IWGRP)
	{
		fprintf (stderr, "w");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IXGRP)
	{
		fprintf (stderr, "x");
	}
	else
	{
		fprintf (stderr, "-");
	}

	// alle anderen Benutzter
	if (pAttribut->st_mode & S_IROTH)
	{
		fprintf (stderr, "r");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IWOTH)
	{
		fprintf (stderr, "w");
	}
	else
	{
		fprintf (stderr, "-");
	}

	if (pAttribut->st_mode & S_IXOTH)
	{
		fprintf (stderr, "x");
	}
	else
	{
		fprintf (stderr, "-");
	}

	fprintf (stderr, "\n");
} /* usbevent_listRights */


/**************************************************************************//**
 *
 *	\brief		usbevent_listDirectory
 *
 *	\details	listet den Inhalt des übergebenen Directory auf.
 *
 *	\param		*pPath		Pointer auf Pathname
 *
 *	\return		-
 *
 ******************************************************************************/
static void usbevent_listDirectory (char *pPath)
{
	DIR				*pDir;
	struct dirent	*pDirent;
	struct stat		attribut;


	fprintf (stderr, "usbevent_listDirectory: path: %s, ", pPath);

	stat (pPath, &attribut);
	usbevent_listRights (&attribut);

	if (chdir (pPath) == 0)
	{
		fprintf (stderr, "\tchange to directory %s\n", pPath);
	}
	else
	{
		fprintf (stderr, "\tCouldn't change to directory\n");
	} // else if (chdir (pPath) == 0)

	pDir = opendir ("./");

	if (pDir != NULL)
	{
		while (pDirent = readdir (pDir))
		{
			fprintf (stderr, "\t- %s\t", pDirent->d_name);
			stat (pDirent->d_name, &attribut);
			usbevent_listRights (&attribut);
		} // while (pDirent = readdir (pDir))

		(void) closedir (pDir);
	}
	else
	{
		fprintf (stderr, "\tls Couldn't open the directory\n");
	} // else if (pDir != NULL)
} /* usbevent_listDirectory  */


/**************************************************************************//**
 *
 *	\brief		usbevent_logEvent
 *
 *	\details	schreibt die Eventnummer, welche dem PA Index entspricht in die
 *				EV Tabelle.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void usbevent_logEvent (ts_db_connect		*pConnect,
							   te_db_PAidOffsetAi	idOffset)
{
	ts_db_ev	dbEV;


	// PA Index ermitteln und in Eventliste eintragen
	snprintf (dbEV.strValue, C_db_dataStringSize, "%d", C_db_paBaseUSB + idOffset);
	snprintf (dbEV.strDateTime, C_db_dataStringSize, "%s",  "");
	db_rowAdd (pConnect, E_db_tableTypeEV, E_db_listTypeNone, 0, &dbEV);

	fprintf (stderr, "usbevent_logEvent: EV:%s\n", dbEV.strValue);
} /* usbevent_logEvent */


/**************************************************************************//**
 *
 *	\brief		usbevent_mailEvent
 *
 *	\details	mailt den Text der Textparameternummer.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *	\param		idOffset	Offset für Event Wert
 *
 *	\return		-
 *
 ******************************************************************************/
static void usbevent_mailEvent (ts_db_connect		*pConnect,
								ts_mail				*pMail,
								te_db_PAidOffsetUSB	idOffset)
{
	ts_db_tp	dbTP;


	// TP Index ermitteln und in mailmessage eintragen
	dbTP.id = pMail->languageOffset + C_db_paBaseUSB + idOffset;
	db_rowGet (pConnect, E_db_tableTypeTP, E_db_listTypeNone, 0, &dbTP);

	snprintf (pMail->message, C_db_dataStringSize, "%s", dbTP.strText);
	mail_send (pMail->from, pMail->to, pMail->subject, pMail->message);

	fprintf (stderr, "usbevent_mailEvent: TP:%s\n", dbTP.strText);
} /* usbevent_mailEvent */


/**************************************************************************//**
 *
 *	\brief		usbevent_checkUploads
 *
 *	\details	prüft den USB Memorystick ob die Bedingungen für eventuelle
 *				Uploads erfüllt sind:
 *					- passwort vorhanden und korrekt?
 *					- paraup vorhanden?
 *					- textup vorhanden?
 *				Trifft dies zu, werden die entsprechenden Uploads ausgeführt
 *				und in der EV Tabelle gelogt. Anschliessend werden die
 *				Steuerfiles auf dem USB-Stick gelöscht, für ioLog eine
 *				Parameterübernahme initiiert und ein Neustart der Systems
 *				veranlasst.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *	\param		*pC4lusb	Pointer auf Struktur ts_c4lusb
 *	\param		*pPath		Pointer auf Pfadstring
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static int usbevent_checkUploads (ts_db_connect	*pConnect,
								  ts_mail		*pMail,
								  ts_c4lusb		*pC4lusb,
								  char			*pPath)
{
	FILE	*pFilePassword;
	FILE	*pFileControl;
	char	pathAndFilename [C_db_dataStringSize];
	char	commandStr [C_db_dataStringSize];
	char	password [C_db_dataStringSize];
	int		rebootRequired = 0;

	// notwendig für Funktion getline
	char	 *pPassword = password;
	size_t	 stringSize = C_db_dataStringSize;
	int		 stringLen;


	fprintf (stderr, "usbevent_checkUploads:\n");
	usbevent_listDirectory (pPath);
	snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenamePassword);
	fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

	if (pFilePassword = fopen (pathAndFilename, "r"))
	{
		// Passwortfile ist vorhanden, Passwort lesen
		stringLen = getline (&pPassword, &stringSize, pFilePassword);
		ptkC4l_replaceChar1withChar2 (password, '\r', '\0');

		if (stringLen > 0)
		{
			fprintf (stderr, "\tpassword in file: '%s'\n", password);
			fprintf (stderr, "\tpassword in PA  : '%s'\n", pC4lusb->strPassword);

			if (strcmp (password, pC4lusb->strPassword) == 0)
			{
				fprintf (stderr, "\tpassword correct\n");

				// Upload Parameter?
				snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableParamUpload);
				fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

				if (pFileControl = fopen (pathAndFilename, "r"))
				{
					snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenameParamUpload);
					fprintf (stderr, "\t\033[32mread file %s\033[0m\n", pathAndFilename);

					// Zeichencodierung wdTw zu utf8 konvertieren
					snprintf (commandStr, C_db_dataStringSize, "/opt/11007/conv2utf8 -f %s", pathAndFilename);
					system (commandStr);

					snprintf (commandStr, C_db_dataStringSize, "mv -f %s.utf8 %s", pathAndFilename, pathAndFilename);
					system (commandStr);
					
					db_csvReadFile (pConnect, E_db_tableTypePA, pathAndFilename);

					usbevent_logEvent (pConnect, E_db_PAoUSBSMTPuploadPA);

					if (pC4lusb->msgSMTPuploadPA)
					{
						usbevent_mailEvent (pConnect, pMail, E_db_PAoUSBSMTPuploadPA);
					} // if (pC4lusb->msgSMTPuploadPA)

					fclose (pFileControl);

					snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableParamUpload);
					remove (pathAndFilename);
					fprintf (stderr, "\tdelete file %s\n", pathAndFilename);

					rebootRequired = 1;
				}
				else
				{
					fprintf (stderr, "\t\033[32mno param upload\033[0m\n");
				} // else if (pFile = fopen (pathAndFilename, "r"))

				// Upload Textparameter?
				snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableTextUpload);
				fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

				if (pFileControl = fopen (pathAndFilename, "r"))
				{
					snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenameTextUpload);
					fprintf (stderr, "\t\033[32mread file %s\033[0m\n", pathAndFilename);

					// Zeichencodierung wdTw zu utf8 konvertieren
					snprintf (commandStr, C_db_dataStringSize, "/opt/11007/conv2utf8 -f %s", pathAndFilename);
					system (commandStr);

					snprintf (commandStr, C_db_dataStringSize, "mv -f %s.utf8 %s", pathAndFilename, pathAndFilename);
					system (commandStr);
					
					db_csvReadFile (pConnect, E_db_tableTypeTP, pathAndFilename);

					usbevent_logEvent (pConnect, E_db_PAoUSBSMTPuploadTP);

					if (pC4lusb->msgSMTPuploadTP)
					{
						usbevent_mailEvent (pConnect, pMail, E_db_PAoUSBSMTPuploadTP);
					} // if (pC4lusb->msgSMTPuploadPA)

					fclose (pFileControl);

					snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableTextUpload);
					remove (pathAndFilename);
					fprintf (stderr, "\tdelete file %s\n", pathAndFilename);

					rebootRequired = 1;
				}
				else
				{
					fprintf (stderr, "\tno text upload\n");
				} // else if (pFile = fopen (pathAndFilename, "r"))
			}
			else
			{
				fprintf (stderr, "\tpassword incorrect\n");
			} // else if (strcmp (password, pC4lusb->strPassword) == 0)
		}
		else
		{
			fprintf (stderr, "\tpassword file empty\n");
		} // if (stringLen > 0)

		fclose (pFilePassword);
	}
	else
	{
		fprintf (stderr, "\tpassword file not found\n");
	} // else if (pFile = fopen (pathAndFilename, "r"))

	return (rebootRequired);
} /* usbevent_checkUploads */


/**************************************************************************//**
 *
 *	\brief		usbevent_checkDownloads
 *
 *	\details	prüft den USB Memorystick ob die Bedingungen für eventuelle
 *				Downloads erfüllt sind:
 *					- paradown vorhanden?
 *					- textdown vorhanden?
 *			    Trifft dies zu, werden die entsprechenden Downloads ausgeführt
 *				und in der EV Tabelle gelogt.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *	\param		*pC4lusb	Pointer auf Struktur ts_c4lusb
 *	\param		*pPath		Pointer auf Pfadstring
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static void usbevent_checkDownloads (ts_db_connect	*pConnect,
									 ts_mail		*pMail,
									 ts_c4lusb		*pC4lusb,
									 char			*pPath)
{
	FILE	*pFile;
	char	pathAndFilename [C_db_dataStringSize];


	fprintf (stderr, "usbevent_checkDownloads:\n");
	snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableParamDownload);
	fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

	if (pFile = fopen (pathAndFilename, "r"))
	{
		snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenameParamDownload);
		fprintf (stderr, "\t\033[32mwrite file %s\033[0m\n", pathAndFilename);
		db_csvWriteFile (pConnect, E_db_tableTypePA, pathAndFilename);

		usbevent_logEvent (pConnect, E_db_PAoUSBSMTPdownloadPA);

		if (pC4lusb->msgSMTPdownloadPA)
		{
			usbevent_mailEvent (pConnect, pMail, E_db_PAoUSBSMTPdownloadPA);
		} // if (pC4lusb->msgSMTPdownladTP)

		fclose (pFile);
	}
	else
	{
		fprintf (stderr, "\t\033[32mno param download\033[0m\n");
	} // else if (pFile = fopen (pathAndFilename, "r"))


	snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableTextDownload);
	fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

	if (pFile = fopen (pathAndFilename, "r"))
	{
		snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenameTextDownload);
		fprintf (stderr, "\t\033[32mwrite file %s\033[0m\n", pathAndFilename);
		db_csvWriteFile (pConnect, E_db_tableTypeTP, pathAndFilename);

		usbevent_logEvent (pConnect, E_db_PAoUSBSMTPdownloadTP);

		if (pC4lusb->msgSMTPdownloadTP)
		{
			usbevent_mailEvent (pConnect, pMail, E_db_PAoUSBSMTPdownloadTP);
		} // if (pC4lusb->msgSMTPdownladTP)

		fclose (pFile);
	}
	else
	{
		fprintf (stderr, "\t\033[32mno text download\033[0m\n");
	} // else if (pFile = fopen (pathAndFilename, "r"))

	snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_fileToEnableDataDownload);
	fprintf (stderr, "\tpathAndFilename: %s\n", pathAndFilename);

	if (pFile = fopen (pathAndFilename, "r"))
	{
		snprintf (pathAndFilename, C_db_dataStringSize, "%s/%s", pPath, C_c4lusb_filenameDataDownload);
		fprintf (stderr, "\t\033[32mwrite file %s\033[0m\n", pathAndFilename);
		db_csvWriteDataLists (pConnect, pathAndFilename);

		fclose (pFile);
	}
	else
	{
		fprintf (stderr, "\t\033[32mno datalist download\033[0m\n");
	} // else if (pFile = fopen (pathAndFilename, "r"))
} /* usbevent_checkDownloads */


/**************************************************************************//**
 *
 *	\brief		usbevent_init
 *
 *	\details	initialisiert die USB Daten gemäss Datenbank.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *	\param		*pMail		Pointer auf Struktur ts_mail
 *	\param		*pC4lusb	Pointer auf Struktur ts_c4lusb
 *
 *	\return		mysql_errno
 *
 ******************************************************************************/
static int usbevent_init (ts_db_connect	*pConnect,
						  ts_mail		*pMail,
						  ts_c4lusb		*pC4lusb)
{
	int	retValue = db_connectInit (pConnect,
								   C_db_dataBaseName,
								   C_db_dataBaseHost,
								   C_db_dataBaseUser,
								   C_db_dataBasePassword);


	fprintf (stderr, "db_connectInit: %d\n", retValue);

	if (retValue == 0)
	{
		retValue = c4lusb_configRead (pConnect, pC4lusb);
		fprintf (stderr, "usbevent_init: c4lusb_configRead: %d\n", retValue);

		retValue = mail_configRead (pConnect, pMail);
		fprintf (stderr, "usbevent_init: mail_configRead: %d\n", retValue);

		mail_initSubject (pConnect, pMail);
	} // if (retValue == 0)

	return (retValue);
} /* usbevent_init */


/**************************************************************************//**
 *
 *	\brief		usbevent_remount
 *
 *	\details	unmounted den erkannten USB Memorystick und mounted ihn neu
 *				mit Schreib- unf Leserechten.
 *
 *	\param		-
 *
 *	\return		found, 1:found, 0:not found
 *
 ******************************************************************************/
static char usbevent_remount (void)
{
	char	command [C_db_dataStringSize];
	char	device []	= "/dev/sda1";
	char	letter		= 'a';
	char	num			= '1';
	char	posLetter	= 7;
	char	posNum		= 8;
	char	found		= 0;
	int		result; 


	fprintf (stderr, "usbevent_remount:\n");

	fprintf (stderr, "\t- sudo umount /media/v4l/disk\n");
	system ("sudo umount /media/v4l/disk");

	result = system ("sudo mkdir /media/fat32");
	fprintf (stderr, "\t- sudo mkdir /media/fat32 (result: %d)\n", result);

	// device suchen
	while (!found && letter <= 'z')
	{
		num = '1';

		while (!found && num <= '9')
		{
			device [posLetter]	= letter;
			device [posNum]		= num;

			fprintf (stderr, "\t- look for device %s\n", device);

			if (access (device, F_OK) == 0)
			{
				// device existiert
				found = 1;
				fprintf (stderr, "\t- found device %s\n", device);
			} // if (access (device, F_OK) == 0)

			num++;
		} // while (search && num <= 9)

		letter++;
	} // while (search && letter <= 'z')

	if (found)
	{
		snprintf (command, C_db_dataStringSize, "sudo mount -t vfat -o utf8,umask=000,gid=46,noatime %s /media/fat32", device);
		fprintf (stderr, "\t- %s", command);
		system (command);
	}
	else
	{
		fprintf (stderr, "\t- no device found\n");
	} // else if (search)

	return (found);
} /* usbevent_remount */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *	\brief		main
 *
 *	\details	Zusatzprogramm für den I/O-Logger des techdock Projekts control4log.
 *				Folgende Aufgaben werden ausgeführt:
 *				- öffnen der Datenbank.
 *				- lesen der USB Konfiguration aus der Datenbank.
 *				- ausführen eventueller Uploads vom USB Massenspeicher in die
 *				  Datenbank.
 *				- ausführen eventueller Downloads von der Datenbank zum USB
 *				  Massenspeicher.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	ts_db_connect	connect;
	ts_mail			mail;
	ts_c4lusb		c4lusb;
	char			path [C_db_dataStringSize];
	int				rebootRequired = 0;


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_usbevent_appName, C_usbevent_appVersion, getlogin ());

	// stderr auf /tmp/usbevent.err umleiten

	freopen ("/tmp/usbevent.err", "w", stderr);
	fprintf (stderr, "stderr output to /tmp/usbevent.err\n");

	if (usbevent_init (&connect, &mail, &c4lusb) == 0)
	{
		while (1)
		{
			// Memorystick erkannt?
			if (db_flagGet (&connect, E_db_flagMemoryStickPlugged))
			{
				fprintf (stderr, "USB stick plugged\n");

				// warten bis Memorystick vom System eingebunden
				sleep (C_c4lusb_sleep2Sec);

				// usbevent wird als user c4l gestartet, remount trotzdem nötig

				// USB Massenspeicher mit Schreibrechten remounten
				// mit usbevent_remount wird der USB Massenspeicher in /media/fat32 gemountet
				if (usbevent_remount ())
				{
					snprintf (path, C_db_dataStringSize, "/media/fat32/%s", c4lusb.strDirectory);
					fprintf (stderr, "Pathname to USB Memorystick: %s\n", path);

					usbevent_checkDownloads (&connect, &mail, &c4lusb, path);
					rebootRequired = usbevent_checkUploads (&connect, &mail, &c4lusb, path);

					fprintf (stderr, "\t- sudo umount /media/v4l/disk\n");
					system ("sudo umount /media/v4l/disk");

					// alles erledigt, flag löschen
					db_flagClear (&connect, E_db_flagMemoryStickPlugged);
				} // if (usbevent_remount ())

				if (rebootRequired)
				{
					fprintf (stderr, "\t- reboot required, preparing ...\n");
					db_flagSet (&connect, E_db_flagReconfigEthernet);
					db_flagSet (&connect, E_db_flagReconfigNTP);
					db_flagSet (&connect, E_db_flagReboot);
				} // if (rebootRequired)
			} // if (ioLog_getFlag (&connect, E_db_flagReboot)
			
			sleep (C_c4lusb_sleep2Sec);
		} // while (1)
	}
	else
	{
		fprintf (stderr, "usbevent_init failed\n");
	} // else if (usbevent_init (&connect, &mail, &c4lusb) == 0)

	fprintf (stderr, "end usbevent\n");
	fclose (stderr);

	return (0);
} /* main */


/* usbevent */
