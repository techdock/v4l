/**************************************************************************//**
 *
 *	\file		shutdown.c
 *
 *	\brief		Startet das c4l neu mit dem cmd FC99
 *
 *	\details	-
 *
 *	\date		05.04.2023
 *	\author		Michael Mislin
 *	\copyright	TECHDOCK GmbH
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	05.04.23  mimi		- Erstellt
 *				01.08.23  mimi		- Email Einstellungen nach Neustart machen
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buildDate.h"
#include "db.h"
#include "mail.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_commandStrLen					64

const uint8_t C_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
int	first_startup	= 0;	//!< um zu erkennen ob ein Neustart ausgeführt wurde

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/ 

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	reboot "shutdonw -r now" vom Pi über c4l-interface mit FC99
 *				Email Einstellungen nach jedem Neustart machen
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	char			commandStr [C_commandStrLen] = "";

	ts_db_connect	connect;
	ts_mail			mail;

	// immer auf Konsole ausgeben
	fprintf (stderr, "shutdown - start version %s, as user %s\n", C_appVersion, getlogin ());
	fprintf (stderr,"shutdown - open database %s\n", C_db_dataBaseName);
	
	while (1)
	{
	sleep(1);
//	fprintf (stderr,"- timeout 5s\n"); // Debug
	
		// Datenbank initialisieren
		if (db_connectInit (&connect,
							C_db_dataBaseName,
							C_db_dataBaseHost,
							C_db_dataBaseUser,
							C_db_dataBasePassword))
		{
			fprintf (stderr,"shutdown - could not initialize MySQL client library\n");
		}
		else
		{

			//write current system time into VA5, -6 & -7
			db_vaUpdateTime (&connect);

			//write email setttings after reboot just one time
			if (first_startup == 0)
			{
				mail_configRead (&connect, &mail);
				mail_configDebug (&mail);
				mail_configWriteToMsmtprc (&mail);
				mail_initSubject (&connect, &mail);

				//only do email settings one's after rebooting
				first_startup = 1;
			}

			if (db_flagGet (&connect, E_db_flagReboot))
			{
				sleep(1);
				//Rebooting when flag is true
				fprintf (stderr,"shutdown - rebooting by interface command FC99 \n"); 

				//Flag zurücksetzen
				db_flagClear (&connect, E_db_flagServiceMode);
				db_flagClear (&connect, E_db_flagReboot);
				
				//Reboot ausführen
				snprintf (commandStr, C_commandStrLen, "sudo shutdown -r now");
				fprintf (stderr,"\n- %s\n", commandStr);
				system (commandStr);
			}
			else
			{
//				fprintf (stderr,"- flag reboot not set\n"); // Debug
			} // else if (db_flagGet (&connect, E_db_flagReboot))
		} // if (mysql_library_init (0, NULL, NULL))

	}

			fprintf (stderr,"shutdown - exiting\n\n");
	return (0);
} /* main */

/* shutdown.c */

