/**************************************************************************//**
 *
 *	\file		configntp.c
 *
 *	\brief		konfiguriert den Network Time Protocol (ntp) Client anhand der 
 * 				Informationen in der Datenbank.
 *
 *	\details	-
 *
 *	\date		20.08.2021
 *	\author		Claudio Häusermann
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	20.08.21 ch
 *					- Erstellt.
 *	\version	07.12.21 mm
 *					- C_appVersion über builDate.h implementiert.
 *					- Einträge von timesyncd-template.conf wurden trotz
 *					  fopen (fileName, "a+") überschrieben. Funktioniert nur
 *					  mit fopen (fileName, "a").
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buildDate.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_commandStrLen					64

#define C_timesyncd_templateFileName	"timesyncd-template.conf"
#define C_timesyncd_fileName			"timesyncd.conf"
#define C_timesyncd_directory			"/etc/systemd/"
#define C_timesyncd_fallbackNtpStr		"0.debian.pool.ntp.org 1.debian.pool.ntp.org 2.debian.pool.ntp.org 3.debian.pool.ntp.org"
#define C_timesyncd_rootDistanceMaxSec	5
#define C_timesyncd_pollIntervalMinSec	32
#define C_timesyncd_pollIntervalMaxSec	2048

const uint8_t C_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/ 

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	konfiguriert die drei eth Schnittstellen gemäss Datenbankeintrag.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	char			commandStr [C_commandStrLen] = "";
	char			ntpHostStr [C_commandStrLen] = "";

	ts_db_connect	connect;
	ts_db_pa		dbPA;
	FILE 			*fptr;
	int				ntpPort = 0;

	// immer auf Konsole ausgeben
	fprintf (stderr, "start configntp version %s, as user %s\n", C_appVersion, getlogin ());
	fprintf (stderr,"- open database %s\n", C_db_dataBaseName);

	// Datenbank initialisieren
	if (db_connectInit (&connect,
						C_db_dataBaseName,
						C_db_dataBaseHost,
						C_db_dataBaseUser,
						C_db_dataBasePassword))
	{
		fprintf (stderr,"- could not initialize MySQL client library\n");
	}
	else
	{
		if (db_flagGet (&connect, E_db_flagReconfigNTP))
		{
			// Template kopieren und in timesyncd.conf umbenennen
			snprintf (commandStr, C_commandStrLen, "sudo cp %s %s", C_timesyncd_templateFileName, C_timesyncd_fileName);
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
			sleep(1);

			// File öffnen im append mode
//				fptr = fopen(C_timesyncd_fileName,"a+");
			fptr = fopen(C_timesyncd_fileName,"a");
			
			if(fptr == NULL)
			{
				fprintf (stderr,"Fehler beim Öffnen der Datei \r\n");             
			} // if(fptr == NULL)
			else
			{	
				dbPA.id = C_db_paBaseNTP + E_db_PAoNTPPort;

				if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
				{
					ntpPort = atoi(dbPA.strValue); 
					dbPA.id = C_db_paBaseNTP + E_db_PAoNTPHost;

					if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
					{
						// strncpy(ntpHostStr, dbPA.strValue,C_commandStrLen); Funktion unsicher
						snprintf (ntpHostStr, C_commandStrLen, dbPA.strValue);
						
						fprintf(fptr,"NTP=%s\r\n",ntpHostStr);
						fprintf(fptr,"FallbackNTP=%s\r\n",C_timesyncd_fallbackNtpStr);
						fprintf(fptr,"RootDistanceMaxSec=%d\r\n",C_timesyncd_rootDistanceMaxSec);
						fprintf(fptr,"PollIntervalMinSec=%d\r\n",C_timesyncd_pollIntervalMinSec);
						fprintf(fptr,"PollIntervalMaxSec=%d\r\n",C_timesyncd_pollIntervalMaxSec);
					} // if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
					else
					{
						fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
					} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
				} // if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
				else
				{
					fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
				} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0
			} // else if(fptr == NULL)

			fclose(fptr);

			//Datei verschieben und überschreiben
			snprintf (commandStr, C_commandStrLen, "sudo mv -f %s %s%s", C_timesyncd_fileName, C_timesyncd_directory, C_timesyncd_fileName);
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
			sleep(1);
			
			// timesyncd.conf editierbar machen
			snprintf (commandStr, C_commandStrLen, "sudo chmod 777 %s%s", C_timesyncd_directory, C_timesyncd_fileName);
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
			
			// ntp aktiv oder inaktive setzen
			if (ntpPort == 0)
			{
				snprintf (commandStr, C_commandStrLen, "sudo timedatectl set-ntp false");
			} // if (ntpPort == 0)
			else
			{
				snprintf (commandStr, C_commandStrLen, "sudo timedatectl set-ntp true");
			} // else: if (ntpPort == 0)
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
			
			db_flagClear (&connect, E_db_flagReconfigNTP);

			// Reboot ausführen
			snprintf (commandStr, C_commandStrLen, "sudo shutdown -r now");
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
		}
		else
		{
			fprintf (stderr,"- flag reconfig ntp not set\n");
		} // else if (db_flagGet (&connect, E_db_flagReconfigNTP))
	} // if (mysql_library_init (0, NULL, NULL))

	fprintf (stderr,"- done\n\n");
	return (0);
} /* main */

/* configntp.c */

