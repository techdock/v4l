 /**************************************************************************//**
 *
 *	\file		dbdelete.c
 *
 *	\brief		Löscht eine bestehende Datenbank.
 *
 *	\details	-
 *
 *	\date		20.08.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	20.08.21 mm
 *					- Erstellt.
 *	\version	21.12.21 mm
 *					- C__dbdelete_appVersion über builDate.h implementiert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdio.h>

#include "buildDate.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_dbdelete_appName		"dbdelete"

const char C_dbdelete_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	erstellt / initialisiert die mySQL Datenbank mit allen Datanlisten.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	ts_db_connect	connect;


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_dbdelete_appName, C_dbdelete_appVersion, getlogin ());

	if (db_connectInit (&connect,
						C_db_dataBaseName,
						C_db_dataBaseHost,
						C_db_dataBaseUser,
						C_db_dataBasePassword) == 0)
	{
		fprintf (stderr, "MySQL client version: %s\n", mysql_get_client_info ());

		if (db_delete (&connect))
		{
			fprintf (stderr, "failed\n");
		}
		else
		{
			fprintf (stderr, "ok\n");
		}

		mysql_library_end ();
	}
	else
	{
		printf ("could not initialize MySQL client library\n");
	} // else if (db_connectInit (&connect, ...

	return (0);
} /* main */

/* dbdelete */

