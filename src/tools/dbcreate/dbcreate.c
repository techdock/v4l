 /**************************************************************************//**
 *
 *	\file		dbcreate.c
 *
 *	\brief		Erstellt eine neue Datenbank.
 *
 *	\details	dbcreate erstellt eine neue Datenbank mit folgenden Tabellen:
 *				- AI		aktuelle analoge binäre Eingangswerte 
 *				- AO		aktuelle analoge binäre Ausgangswerte 
 *				- DI		aktuelle digitale Eingangswerte 
 *				- DO		aktuelle digitale Ausgangswerte 
 *				- DLAI1		Datenliste analoge binäre Eingangswerte Eingang 1
 *				- DLAI2		Datenliste analoge binäre Eingangswerte Eingang 2
 *				- DLAO1		Datenliste analoge binäre Ausgangswerte Ausgang 1
 *				- DLAO2		Datenliste analoge binäre Ausgangswerte Ausgang 2
 *				- DLDI1		Datenliste digitale Eingangswerte Eingang 1
 *				- DLDI2		Datenliste digitale Eingangswerte Eingang 2
 *				- DLDI3		Datenliste digitale Eingangswerte Eingang 3
 *				- DLDI4		Datenliste digitale Eingangswerte Eingang 4
 *				- DLDO1		Datenliste digitale Ausgangswerte Ausgang 1
 *				- DLDO2		Datenliste digitale Ausgangswerte Ausgang 2
 *				- DLDO3		Datenliste digitale Ausgangswerte Ausgang 3
 *				- DLDO4		Datenliste digitale Ausgangswerte Ausgang 4
 *				- DLVA1		Datenliste analoge skalierte Eingangswerte Eingang 1
 *				- DLVA2		Datenliste analoge skalierte Eingangswerte Eingang 2
 *				- DLVA3		Datenliste analoge skalierte Ausgangswerte Ausgang 1
 *				- DLVA4		Datenliste analoge skalierte Ausgangswerte Ausgang 2
 *				- EV		Events
 *				- FLAG		Flags für programminternen Gebrauch
 *				- PA		Parameter
 *				- TP		Textparameter
 *				- VA		aktuelle skalierte Prozessvariablen 
 *
 *	\date		20.08.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	20.08.21 mm
 *					- Erstellt.
 *	\version	26.08.21 mm
 *					- Alle Einträge der Tabelle VA mit 0 initialisieren.
 *	\version	17.12.21 mm
 *					- C__dbcreate_appVersion über builDate.h implementiert.
 *					- Tabelle FLAG um Spalte description erweitert.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <stdio.h>
#include <unistd.h>

#include "buildDate.h"
#include "csv.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_dbcreate_appName		"dbcreate"

const char C_dbcreate_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};

// Textdefinitionen zu te_db_flag;
const char *C_dbcreate_flagStr [E_db_flagCount] = {
	"Service Mode",
	"Init Eventlist (FC30)",
	"Init Parameter (FC32)",
	"Reconfig Ethernet (FC33)",
	"Reconfig NTP (FC34)",
	"Reboot (FC99)",
	"Memorystick Plugged",
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		dbcreate_createDb
 *
 *  \details	erstell eine Datenbank gemäss dem Inhalt von pConnect.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void dbcreate_createDb (ts_db_connect *pConnect)
{
	fprintf (stderr, "create %s - ", C_db_dataBaseName);
	
	if (db_create (pConnect))
	{
		fprintf (stderr, "failed\n");
	}
	else
	{
		fprintf (stderr, "ok\n");
	}
} /* dbcreate_createDb */


/**************************************************************************//**
 *
 *  \brief		dbcreate_createAllTables
 *
 *  \details	erstellt alle Tabellen (ohne Wrapper Funktionen) gemäss dem Typ
 *				te_db_tableType in der Datenbank pConnect.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
static void dbcreate_createAllTables (ts_db_connect *pConnect)
{
	te_db_tableType	tableType;
	int				retValue;

	for (tableType = E_db_tableTypeAI; tableType < E_db_tableTypeCount; tableType++)
	{
		if (tableType != E_db_tableTypeDL)
		{
			fprintf (stderr, "create table %s - ", C_db_tableTypeStr [tableType]);

			retValue = db_tableCreate (pConnect, tableType);

			if (retValue)
			{
				fprintf (stderr, "failed, error: %d\n", retValue);
			}
			else
			{
				fprintf (stderr, "ok\n");
			} // else if (db_tableCreate (pConnect, tableType))
		} // if (tableType != E_db_tableTypeDL)
	} // for (tableType = E_db_tableTypePA; tableType < E_db_tableTypeCount; tableType++)
} /* dbcreate_createAllTables */


/**************************************************************************//**
 *
 *  \brief		dbcreate_initFlags
 *
 *  \details	initialisiert die Flags Tabelle. Alle Flags werden dabei auf 0
 *				(Funktion aus) initialisiert.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
void static dbcreate_initFlags (ts_db_connect *pConnect)
{
	ts_db_flag	dbFlag;

	fprintf (stderr, "initialize table FLAGS\n");

	for (dbFlag.id = E_db_flagServiceMode; dbFlag.id < E_db_flagCount; dbFlag.id++)
	{
		dbFlag.value = 0;
		snprintf (dbFlag.strDescription, C_db_dataStringSize, C_dbcreate_flagStr [dbFlag.id]);
		fprintf (stderr, "\t- id:%d\tdescription:%s\n", dbFlag.id, dbFlag.strDescription);
		
		db_rowAdd (pConnect, E_db_tableTypeFLAG, 0, 0, &dbFlag);
	} // for (dbFlag.id = E_db_flagServiceMode; dbFlag.id < E_db_flagCount; dbFlag.id++)
} /* dbcreate_initFlags */


/**************************************************************************//**
 *
 *  \brief		dbcreate_initVA
 *
 *  \details	initialisiert die VA Tabelle mit 0.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
void static dbcreate_initVA (ts_db_connect *pConnect)
{
	ts_db_va dbVA;

	fprintf (stderr, "initialize table VA\n");

	for (dbVA.id = E_db_VAidAi1Scaled; dbVA.id < E_db_VAidCount - 1; dbVA.id++)
	{
		snprintf (dbVA.strDescription, C_db_dataStringSize, "0"); 

		db_rowAdd (pConnect, E_db_tableTypeVA, 0, 0, &dbVA);
	} // for (dbVA.id = E_db_VAidAi1Scaled; dbVA.id < E_db_VAidCount - 1; dbVA.id++)
} /* dbcreate_initVA */


/**************************************************************************//**
 *
 *  \brief		dbcreate_csvReadPA
 *
 *  \details	liest die Daten aus C_db_csvPaDefaultFile und erstellt damit
 *				die Tabelle PA in der Datenbank pConnect.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
void static dbcreate_csvReadPA (ts_db_connect *pConnect)
{
	fprintf (stderr, "initialize table PA with %s - ", C_db_csvPaDefaultFile);
	
	if (db_csvReadFile (pConnect, E_db_tableTypePA, C_db_csvPaDefaultFile))
	{
		fprintf (stderr, "failed\n");
	}
	else
	{
		fprintf (stderr, "ok\n");
	}
} /* dbcreate_csvReadPA */


/**************************************************************************//**
 *
 *  \brief		dbcreate_csvReadTP
 *
 *  \details	liest die Daten aus C_db_csvTpDefaultFile und erstellt damit
 *				die Tabelle TP in der Datenbank pConnect.
 *
 *	\param		*pConnect	Pointer auf Struktur ts_db_connect
 *
 *	\return		-
 *
 ******************************************************************************/
void static dbcreate_csvReadTP (ts_db_connect *pConnect)
{
	fprintf (stderr, "initialize table TP with %s - ", C_db_csvTpDefaultFile);

	if (db_csvReadFile (pConnect, E_db_tableTypeTP, C_db_csvTpDefaultFile))
	{
		fprintf (stderr, "failed\n");
	}
	else
	{
		fprintf (stderr, "ok\n");
	}
} /* dbcreate_csvReadTP */


/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	erstellt / initialisiert die mySQL Datenbank mit allen Datanlisten.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	ts_db_connect	connect;


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_dbcreate_appName, C_dbcreate_appVersion, getlogin ());

	if (db_connectInit (&connect,
						C_db_dataBaseName,
						C_db_dataBaseHost,
						C_db_dataBaseUser,
						C_db_dataBasePassword) == 0)
	{
		fprintf (stderr, "MySQL client version: %s\n", mysql_get_client_info ());

		dbcreate_createDb (&connect);
		dbcreate_createAllTables (&connect);
		dbcreate_initFlags (&connect);
		dbcreate_initVA (&connect);
		dbcreate_csvReadPA (&connect);
		dbcreate_csvReadTP (&connect);

		fprintf (stderr, "create all datalists:\n");

		if (db_tableCreateAllDatalists (&connect))
		{
			fprintf (stderr, "%s only ends succsessfully if %s dosn't exist, please run dbdelete first\n",
								C_dbcreate_appName, C_db_dataBaseName);
		}
		else
		{
			fprintf (stderr, "ok\n");
		} // if (db_tableCreateAllDatalists (&connect) == 0)

		mysql_library_end ();
	}
	else
	{
		printf ("could not initialize MySQL client library\n");
	} // else if (db_connectInit (&connect, ...

	return (0);
} /* main */

/* dbcreate */

