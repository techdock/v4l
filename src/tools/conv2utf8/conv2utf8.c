 /**************************************************************************//**
 *
 *	\file		conv2utf8.c
 *
 *	\brief		stellt die Zeichencodierung eines Files fest und führt eine
 *				Konvertierung nach UTF-8 durch.
 *
 *	\details	-
 *
 *	\date		22.12.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	22.12.21 mm
 *					- Erstellt.
 *
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buildDate.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_conv2utf8_appName		"conv2utf8"

#define	C_conv2utf8_cmdStrLen	256
#define	C_conv2utf8_retStrLen	1024

const char C_conv2utf8_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 
/******************************************************************************
 * External Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/
extern char *optarg;


/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	erstellt / initialisiert die mySQL Datenbank mit allen Datanlisten.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (int	argc,
		  char*	argv [])
{
	char	pPathAndFilename [C_conv2utf8_cmdStrLen] = {'\0'};
	char	cmdStr [C_conv2utf8_cmdStrLen];
	char	retStr [C_conv2utf8_retStrLen];
	char	*pEqualsSign;
	int		option;
	FILE	*pFile;


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_conv2utf8_appName, C_conv2utf8_appVersion, getlogin ());

	// Paramater auswerten
	if (argc < 2)
	{
		fprintf (stderr, "usage: conv2utf8 -f filename\n");
	}
	else
	{
		while ((option = getopt (argc, argv, "f:")) != -1)
		{
			switch (option)
			{
				case 'f':
					snprintf (pPathAndFilename, C_conv2utf8_cmdStrLen, optarg);
					break;

				default:
					fprintf (stderr, "no arguments\n");
					break;
			} // switch (option)
		} // while ((option = getopt (argc, argv, "f:")) != -1)

		if (strlen (pPathAndFilename) > 0)
		{
			// Zeichencodierung des Files ermitteln
			snprintf (cmdStr, C_conv2utf8_cmdStrLen, "file -bi %s", pPathAndFilename);
			pFile = popen (cmdStr, "r");

			while (fgets (retStr, C_conv2utf8_retStrLen, pFile))
			{
				fprintf (stderr, "cmdstr: %s\n", cmdStr);
				break;
			}

			pclose (pFile); 

			// LineFeed durch \0 ersetzen
			pEqualsSign = memchr (retStr, '\n', C_conv2utf8_retStrLen);
			*pEqualsSign = '\0';

			// charset in der Antwort suchen
			pEqualsSign = memchr (retStr, '=', C_conv2utf8_retStrLen);
			
			if (pEqualsSign != NULL)
			{
				*pEqualsSign++;
				snprintf (cmdStr, C_conv2utf8_cmdStrLen, "iconv -f %s -t UTF8 %s -o %s.utf8",
							pEqualsSign, pPathAndFilename, pPathAndFilename);

				fprintf (stderr, "cmdstr: %s\n", cmdStr);
				system (cmdStr);
			} // if (pEqualsSign != NULL)
		} // if (strlen (pPathAndFilename) > 0)
	} // else if (argc < 2)

	fprintf (stderr, "end\n");

	return (0);
} /* main */

/* conv2utf8 */

