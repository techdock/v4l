/**************************************************************************//**
 *
 *	\file		configeths.c
 *
 *	\brief		konfiguriert die Netzwerkschnittstellen anhand der Informationen
 *				in der Datenbank.
 *
 *	\details	-
 *
 *	\date		16.03.2021
 *	\author		Marcel Ming
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	16.03.21 mm
 *					- Erstellt.
 *	\version	10.08.21 mm
 *					- Anpassungen an die aktuelle Umgebung.
 * 	\version	19.08.21 ch
 * 					- Configuration in /etc/network/interfaces für die 
 * 					  Adaptereinstellungen und für die Bridge 
 * 					  von eth1 und eth2
 *	\version	18.11.21 mm
 *					- C_appVersion über builDate.h implementiert.
 *					- detailiertere Ausgaben eingefügt.
 *	\version	26.11.21 mm
 *					- Einträge von interface-template wurden trotz
 *					  fopen (fileName, "a+") überschrieben. Funktioniert nur
 *					  mit fopen (fileName, "a").
 *
 *	\version	06.04.23 mimi	- angepasst für v4l und ETH1 und -2 konfiguration 
 *					  entfernt 
 *
 *	\attention	Zum Erstellen der Ethernet Bridge muss das Tool "bridge-utils"
 * 				installiert werden. 
 * 	\attention	Ausserdem muss das File /etc/iproute2/rt_tables mit weiteren 
 * 				Tabellenbezeichnungen ergänzt werden. (1 rt2, 2 rt3)
 * 	\attention	Im File etc/dhcpcd.conf müssen alle devices per erster Zeile 
 * 				"denyinterfaces eth0 eth1 eth2 wlan0" ausgeschaltet werden.
 *
 ******************************************************************************/


/******************************************************************************
 * Compilerswitches                                                           *
 *                                                                            *
 ******************************************************************************/


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <mysql/mysql.h>
#include <sys/socket.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buildDate.h"
#include "db.h"


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define	C_appName						"configeths"

#define	C_commandStrLen		64
#define	C_ipAddrStrLen		16

#define C_interfaces_templateFileName	"interfaces-template"
#define C_interfaces_fileName			"interfaces"
#define C_interfaces_directory			"/etc/network/"

#define	C_eth_count			3				//!< 0, 1, 2
#define	C_eth_first			0				//!< erste eth welche manipuliert werden soll

#define	C_dbid_eth_offsetAddr			0
#define	C_dbid_eth_offsetSubnetMask		1
#define	C_dbid_eth_offsetGateway		2
#define	C_dbid_eth_offsetSpeed			3

const uint8_t C_appVersion [] = {
   C_buildDate_year_CH0,
   C_buildDate_year_CH1,
   C_buildDate_month_CH0,
   C_buildDate_month_CH1,
   C_buildDate_day_CH0,
   C_buildDate_day_CH1,
   '-',
   C_buildDate_hour_CH0,
   C_buildDate_hour_CH1,
   C_buildDate_min_CH0,
   C_buildDate_min_CH1
};


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
 const int C_dbid_eth_base [C_eth_count] = {
	 C_db_paBaseEth0,
	 C_db_paBaseEth1,
	 C_db_paBaseEth2
 };


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Local Functions                                                            *
 *                                                                            *
 ******************************************************************************/
 
  /**************************************************************************//**
 *
 *  \brief		buildNetIdentifier
 *
 *  \details	Wandelt eine subnet mask in einen cidr Wert. 
 * 				Bsp: 255.255.255.0 -> /24
 *
 *	\param		ipAddress
 *	\return		-
 *
 ******************************************************************************/
void buildNetIdentifier(char* ipAddress, char* netIdentifier)
{
	int ipbytes[4];

	sscanf(ipAddress, "%d.%d.%d.%d", &ipbytes[0], &ipbytes[1], &ipbytes[2], &ipbytes[3]);
	
	snprintf (netIdentifier, C_ipAddrStrLen, "%d.%d.%d.0", ipbytes[0], ipbytes[1], ipbytes[2]);
} /* buildNetIdentifier*/


 /**************************************************************************//**
 *
 *  \brief		toCidr
 *
 *  \details	Wandelt eine subnet mask in einen cidr Wert. 
 * 				Bsp: 255.255.255.0 -> /24
 *
 *	\param		ipAddress
 *	\return		cidr
 *
 ******************************************************************************/
int toCidr(char* ipAddress)
{
	int netmask_cidr = 0;
	int ipbytes[4];

	netmask_cidr=0;
	sscanf(ipAddress, "%d.%d.%d.%d", &ipbytes[0], &ipbytes[1], &ipbytes[2], &ipbytes[3]);

	for (int i=0; i<4; i++)
	{
		switch(ipbytes[i])	
		{
			case 0x80:
				netmask_cidr+=1;
				break;

			case 0xC0:
				netmask_cidr+=2;
				break;

			case 0xE0:
				netmask_cidr+=3;
				break;

			case 0xF0:
				netmask_cidr+=4;
				break;

			case 0xF8:
				netmask_cidr+=5;
				break;

			case 0xFC:
				netmask_cidr+=6;
				break;

			case 0xFE:
				netmask_cidr+=7;
				break;

			case 0xFF:
				netmask_cidr+=8;
				break;

			default:
				return netmask_cidr;
				break;
          } // switch(ipbytes[i])
      } // for (int i=0; i<4; i++)
      return netmask_cidr;
} /* toCidr*/
 

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/

/**************************************************************************//**
 *
 *  \brief		main
 *
 *  \details	konfiguriert die drei eth Schnittstellen gemäss Datenbankeintrag.
 *
 *	\param		-
 *	\return		-
 *
 ******************************************************************************/
int main (void)
{
	char			commandStr [C_commandStrLen] = "";
	char			ipAddrStr [C_ipAddrStrLen] = "";
	char			netmaskStr [C_ipAddrStrLen] = "";
	char			gatewayStr [C_ipAddrStrLen] = "";
	char			netIdentifierStr [C_ipAddrStrLen] = "";
	ts_db_connect	connect;
	ts_db_pa		dbPA;
	ts_db_flag		dbFLAG;
	int				i;
	FILE 			*fptr;
	int				eth1Eth2Mode;


	// immer auf Konsole ausgeben
	fprintf (stderr, "start %s version %s, as user %s\n", C_appName, C_appVersion, getlogin ());
	fprintf (stderr, "- open database %s\n", C_db_dataBaseName);

	// Datenbank initialisieren
	if (db_connectInit (&connect,
						C_db_dataBaseName,
						C_db_dataBaseHost,
						C_db_dataBaseUser,
						C_db_dataBasePassword))
	{
		fprintf (stderr, "- could not initialize MySQL client library\n");
	}
	else
	{
		if (db_flagGet (&connect, E_db_flagReconfigEthernet))
		{
			// Template kopieren und in interfaces umbenennen
			snprintf (commandStr, C_commandStrLen, "sudo cp %s %s", C_interfaces_templateFileName, C_interfaces_fileName);
			fprintf (stderr, "\n- %s\n", commandStr);
			system (commandStr);
			sleep(1);

			// File öffnen im append mode
			fptr = fopen(C_interfaces_fileName,"a");
			
			if(fptr == NULL)
			{
				fprintf (stderr, "error open %s\r\n", C_interfaces_fileName);             
			} // if(fptr == NULL)
			else
			{	
				fprintf (stderr, "open %s\r\n", C_interfaces_fileName);             

				dbPA.id = C_db_paBaseMode2;

				if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
				{
					eth1Eth2Mode = atoi(dbPA.strValue);
					fprintf (stderr, "- PA%d: eth2 mode:%d\n", dbPA.id, eth1Eth2Mode);

					// Konfiguration der Netzwerkadapter schreiben
					fprintf(fptr,"\r\n# Following entries made by configeths (project control 4 log)\n");

					for (i = C_eth_first; i < C_eth_count; i++)
					{
						fprintf (stderr, "\neth%d:\n", i);
						dbPA.id = C_dbid_eth_base [i] + C_dbid_eth_offsetAddr;

						if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
						{
							// strncpy(ipAddrStr, dbPA.strValue, C_ipAddrStrLen); Funktion unsicher
							snprintf (ipAddrStr, C_ipAddrStrLen, dbPA.strValue);
							fprintf (stderr, "- PA%d: ipAddr:%s\n", dbPA.id, ipAddrStr);
							dbPA.id = C_dbid_eth_base [i] + C_dbid_eth_offsetSubnetMask;

							if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
							{
								// strncpy(netmaskStr, dbPA.strValue,C_ipAddrStrLen); Funktion unsicher
								snprintf (netmaskStr, C_ipAddrStrLen, dbPA.strValue);
								fprintf (stderr, "- PA%d: netmask:%s\n", dbPA.id, netmaskStr);
								dbPA.id = C_dbid_eth_base [i] + C_dbid_eth_offsetGateway;

								if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
								{
									// strncpy(gatewayStr, dbPA.strValue,C_ipAddrStrLen); Funktion unsicher
									snprintf (gatewayStr, C_ipAddrStrLen, dbPA.strValue);
									fprintf (stderr, "- PA%d: gateway:%s\n", dbPA.id, gatewayStr);

									if (i == 0)
									{
										fprintf(fptr,"\r\nauto eth%d \r\nallow-hotplug eth%d \r\niface eth%d inet static\r\n",i,i,i);
										fprintf(fptr,"address %s\r\n",ipAddrStr);
										fprintf(fptr,"netmask %s\r\n",netmaskStr);
										fprintf(fptr,"gateway %s\r\n",gatewayStr);

										fprintf (stderr, "\r\nauto eth%d \r\nallow-hotplug eth%d \r\niface eth%d inet static\r\n", i, i, i);
										fprintf (stderr, "address %s\r\n", ipAddrStr);
										fprintf (stderr, "netmask %s\r\n", netmaskStr);
										fprintf (stderr, "gateway %s\r\n", gatewayStr);
									} // if (i == 0)
									else
									{
										if(eth1Eth2Mode == 1)
										{
										/*	fprintf(fptr,"\r\nauto eth%d \r\nallow-hotplug eth%d \r\niface eth%d inet static\r\n",i,i,i);
											fprintf(fptr,"address %s\r\n",ipAddrStr);
											fprintf(fptr,"netmask %s\r\n",netmaskStr);
											buildNetIdentifier(ipAddrStr,netIdentifierStr);
											fprintf(fptr,"post-up ip route add %s/%d dev eth%d src %s table rt%d\r\n",
													netIdentifierStr,toCidr(netmaskStr),i,ipAddrStr,i+1);
											fprintf(fptr,"post-up ip route add default via %s dev eth%d table rt%d\r\n",
													gatewayStr,i,i+1);
											fprintf(fptr,"post-up ip rule add from %s/32 table rt%d\r\n",
													ipAddrStr,i+1);
											fprintf(fptr,"post-up ip rule add to %s/32 table rt%d\r\n",
													ipAddrStr, i+1);

											fprintf (stderr, "\r\nauto eth%d \r\nallow-hotplug eth%d \r\niface eth%d inet static\r\n", i, i, i);
											fprintf (stderr, "address %s\r\n", ipAddrStr);
											fprintf (stderr, "netmask %s\r\n", netmaskStr);
											fprintf (stderr, "post-up ip route add %s/%d dev eth%d src %s table rt%d\r\n",
													 netIdentifierStr, toCidr(netmaskStr), i, ipAddrStr, i + 1);
											fprintf (stderr, "post-up ip route add default via %s dev eth%d table rt%d\r\n",
													 gatewayStr, i, i + 1);
											fprintf (stderr, "post-up ip rule add from %s/32 table rt%d\r\n",
													 ipAddrStr, i + 1);
											fprintf (stderr, "post-up ip rule add to %s/32 table rt%d\r\n",
													 ipAddrStr, i + 1);
										*/
										} // if(eth1Eth2Mode == 1)
										else
										{
										/*	fprintf(fptr,"\r\nauto br0 \r\niface br0 inet static\r\n");
											fprintf(fptr,"address %s\r\n",ipAddrStr);
											fprintf(fptr,"netmask %s\r\n",netmaskStr);
											buildNetIdentifier(ipAddrStr,netIdentifierStr);
											fprintf(fptr,"post-up ip route add %s/%d dev br0 src %s table rt%d\r\n",
													netIdentifierStr,toCidr(netmaskStr),ipAddrStr,i+1);
											fprintf(fptr,"post-up ip route add default via %s dev br0 table rt%d\r\n",
													gatewayStr,i+1);
											fprintf(fptr,"post-up ip rule add from %s/32 table rt%d\r\n",
													ipAddrStr,i+1);
											fprintf(fptr,"post-up ip rule add to %s/32 table rt%d\r\n",
													ipAddrStr, i+1);
											fprintf(fptr,"bridge_ports eth1 eth2\r\n");

											fprintf (stderr, "\r\nauto br0 \r\niface br0 inet static\r\n");
											fprintf (stderr, "address %s\r\n", ipAddrStr);
											fprintf (stderr, "netmask %s\r\n", netmaskStr);
											fprintf (stderr, "post-up ip route add %s/%d dev br0 src %s table rt%d\r\n",
													 netIdentifierStr, toCidr(netmaskStr), ipAddrStr, i + 1);
											fprintf (stderr, "post-up ip route add default via %s dev br0 table rt%d\r\n",
													 gatewayStr, i + 1);
											fprintf (stderr, "post-up ip rule add from %s/32 table rt%d\r\n",
													 ipAddrStr,i + 1);
											fprintf (stderr, "post-up ip rule add to %s/32 table rt%d\r\n",
													 ipAddrStr, i + 1);
											fprintf (stderr, "bridge_ports eth1 eth2\r\n");
										*/
											break;
										} // else if(eth1Eth2Mode == 1)
									}// else if (i == 0)
								} // if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
								else
								{
									fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
								} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
							} // if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
							else
							{
								fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
							} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
						} // if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
						else
						{
							fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
						} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0
					} // for (i = 0; i < C_eth_count; i++)	
				} // if (db_rowGet (&connect, E_db_tableTypePA, E_db_listTypeNone, 0, &dbPA) == 0)
				else
				{
					fprintf (stderr,"cant't read row %d of table PA", dbPA.id);
				} // else if (db_rowGet (&connect, E_db_tableTypePA, &dbPA) == 0)
			} // else if(fptr == NULL)
			fclose(fptr);

			//Datei verschieben und überschreiben
			snprintf (commandStr, C_commandStrLen, "sudo mv -f %s %s%s", C_interfaces_fileName, C_interfaces_directory, C_interfaces_fileName);
			fprintf (stderr, "\n- %s\n", commandStr);
			system (commandStr);
			sleep (1);
			
			// interfaces editierbar machen
			snprintf (commandStr, C_commandStrLen, "sudo chmod 777 %s%s", C_interfaces_directory, C_interfaces_fileName);
			fprintf (stderr,"\n- %s\n", commandStr);
			system (commandStr);
			
			dbFLAG.id = E_db_flagReconfigEthernet;
			dbFLAG.value = 0;

			if (db_rowUpdate (&connect, E_db_tableTypeFLAG, &dbFLAG) == 0)
			{
				// Reboot ausführen
				snprintf (commandStr, C_commandStrLen, "sudo shutdown -r now");
				fprintf (stderr,"\n- %s\n", commandStr);
				system (commandStr);
			} // if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)
			else
			{
				fprintf (stderr,"cant't read row %d of table FLAG", dbFLAG.id); 
			} // else if (db_rowUpdate (pConnect, E_db_tableTypeFLAG, &dbFLAG) == 0)

		} // if (dbFLAG.value == 1)

		else
		{
			fprintf (stderr, "ATTENTION: flag 'ReconfigEthernet' not set, no changes made "); 
		}

		fprintf (stderr,"- done\n\n");
	} // if (mysql_library_init (0, NULL, NULL))

	return (0);
} /* main */

/* configeths.c */

