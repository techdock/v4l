/**************************************************************************//**
 *
 *	\file		tcpss.h
 *
 *	\brief		Headerfile für den TCP/IP Socket  Server
 *
 *	\details	Dieses File beinhaltet vorallem Konfigurationen und Typen-
 * 				Definitionen für den TCP/IP Socket Server (tcpss)
 * 
 *
 *	\date		07.07.2021
 *	\author		Claudio Häusermann
 *	\copyright	TRiMADA AG   CH-5610 Wohlen
 *	\remark		Compiler- / IDE-Version
 *
 *	\version	07.07.21 ch
 *					- Erstellt.
 *
 ******************************************************************************/
#ifndef __tcpss_H
    #define __tcpss_H


/******************************************************************************
 * Modules                                                                    *
 *                                                                            *
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Definitions and Macros                                                     *
 *                                                                            *
 ******************************************************************************/
#define C_tcpss_waitBeforeRetry		100000u		//!< Wartezeit in us bevor ein gescheiterter Vorgang wiederholt wird
#define C_tcpss_waitRead			5u			//!< Wartezeit beim Lesen
#define C_tcpss_backlogLength		1u			//!< Maximal Queue-Länge der anstehenden Verbindungen
#define C_tcpss_bufferLength 		256u		//!< Länge von RX- und TX-Buffer  
#define C_tcpss_tcpPortDataBaesId	111 		//!< Datenbank ID für den Port über welchen die Kommunikation laufen soll
#define C_tcpss_sockaddr 		struct sockaddr	//!< Wird zum Casting von sockaddr_in zu sockaddr verwendet 


/******************************************************************************
 * Typedefs and Structures                                                    *
 *                                                                            *
 ******************************************************************************/
typedef enum {
    E_tcpss_mainState_createSocket,				//!< Ein neuer Socket wird erstellt
    E_tcpss_mainState_bindSocket, 				//!< erstellter Socket an Server Adresse "binden"
    E_tcpss_mainState_listenClient,				//!< Warten bis sich ein Client verbinden möchte
    E_tcpss_mainState_acceptClient,				//!< Verbindung wird mit Client hergestellt
    E_tcpss_mainState_readData,					//!< Daten vom Client lesen
    E_tcpss_mainState_evaluatePtk,				//!< Daten werden vom Protokoll-Modul ausgewertet
    E_tcpss_mainState_writeData,				//!< Anwort an Client senden
    E_tcpss_mainState_closeSocket,				//!< Socket schliessen
    E_tcpss_mainState_count						//!< Anzahl der States
} te_tcpss_mainState;


/******************************************************************************
 * Exported Variables                                                         *
 *                                                                            *
 ******************************************************************************/

/******************************************************************************
 * Exported Functions                                                         *
 *                                                                            *
 ******************************************************************************/



#endif //__tcpss_H

/* End tcpss.h */
